using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.AccessControl;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
namespace iVE.BLL {
    public static class GlobalFunction {
        public static string SaveFile (IFormFile file, string exhId, string returnUrl) {
            string url;
            string filename = Path.GetFileNameWithoutExtension (file.FileName);
            string fileextension = Path.GetExtension (file.FileName);
            filename = filename + DateTime.Now.ToString ("yyyyMMdd_hhmmss") + fileextension;

            string filePath = System.IO.Directory.GetCurrentDirectory () + "\\wwwroot\\Files\\";
            filePath += exhId + "\\";
            // string returnUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            if (!Directory.Exists (filePath)) {
                Directory.CreateDirectory (filePath);
                using (FileStream fileStream = System.IO.File.Create (filePath + filename)) {
                    file.CopyTo (fileStream);
                    fileStream.Flush ();
                }
            } else {
                using (FileStream fileStream = System.IO.File.Create (filePath + filename)) {
                    System.GC.Collect ();
                    System.GC.WaitForPendingFinalizers ();
                    file.CopyTo (fileStream);
                    fileStream.Flush ();
                }
            }
            url = returnUrl + "/File/" + exhId + "/" + filename;
            return url;
        }
        public static void Deletefile (string url, string confId) {
            string materialPath = System.IO.Directory.GetCurrentDirectory () + "\\wwwroot\\Files\\";
            materialPath += confId + "\\";
            string fileName = url.Split ('/').Last ();
            materialPath += fileName;
            if (System.IO.File.Exists (materialPath)) {
                System.IO.File.Delete (materialPath);
            }
        }

        public static async Task<string> SaveFileToS3 (IFormFile file, string exhId, dynamic configuration) {

            string url;
            string accessKey = configuration.GetSection ("AWSConfigs:accessKey").Value;
            string secretKey = configuration.GetSection ("AWSConfigs:secretKey").Value;
            string bucketPath = configuration.GetSection ("AWSConfigs:bucketPath").Value;
            bucketPath += exhId;
            string mainUrl = configuration.GetSection ("AWSConfigs:mainUrl").Value;
            string folderPath = configuration.GetSection ("AWSConfigs:folderPath").Value;

            string filename = Path.GetFileNameWithoutExtension (file.FileName);
            string fileextension = Path.GetExtension (file.FileName);
            filename = filename + DateTime.Now.ToString ("yyyyMMdd_hhmmss") + fileextension;

            var client = new AmazonS3Client (accessKey, secretKey, Amazon.RegionEndpoint.APSoutheast1);

            using (var newMemoryStream = new MemoryStream ()) {
                file.CopyTo (newMemoryStream);
                var uploadRequest = new TransferUtilityUploadRequest {
                    InputStream = newMemoryStream,
                    Key = filename,
                    BucketName = bucketPath,
                    CannedACL = S3CannedACL.PublicRead
                };

                var fileTransferUtility = new TransferUtility (client);
                await fileTransferUtility.UploadAsync (uploadRequest);

                url = mainUrl + folderPath + exhId + "/" + filename;

                return url;
            }
        }

    }
}