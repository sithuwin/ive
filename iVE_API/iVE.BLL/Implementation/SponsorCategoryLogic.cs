using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
namespace iVE.BLL.Implementation {
    public class SponsorCategoryLogic : ISponsorCategoryLogic {
        private IRepositoryWrapper _repositoryWrapper;
        public SponsorCategoryLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
        }

        public dynamic GetSponsorCategory () {
            return _repositoryWrapper.SponsorCategory.GetSponsorCategory ();
        }
    }
}