using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace iVE.BLL.Implementation
{
    public class LeaderBoardLogic: ILeaderBoardLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        public LeaderBoardLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
        }

        public dynamic GetAllLeaderBoard(dynamic obj,string returnUrl){
            return _repositoryWrapper.LeaderBoard.GetAllLeaderBoard(obj,returnUrl);
        }

        public dynamic GetAllEntitlePoints(dynamic obj){
            return _repositoryWrapper.LeaderBoard.GetAllEntitlePoints(obj);
        }
    }
}