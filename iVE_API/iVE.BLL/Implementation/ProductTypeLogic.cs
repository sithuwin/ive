using System.Collections.Generic;
using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
namespace iVE.BLL.Implementation
{
    public class ProductTypeLogic : IProductTypeLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        public ProductTypeLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
        }
        public dynamic GetProductTypeList()
        {
            return _repositoryWrapper.ProductType.GetProductTypeList();
        }
    }
}