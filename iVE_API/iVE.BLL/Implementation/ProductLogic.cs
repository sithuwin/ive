using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
namespace iVE.BLL.Implementation {
    public class ProductLogic : IProductLogic {
        private IRepositoryWrapper _repositoryWrapper;
        public ProductLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
        }

        public string CreateOrUpdateProductByExhId (dynamic obj, string exhId, string productId, ref string id) {
            string seqno = obj.seqNo;
            if (string.IsNullOrEmpty (seqno)) {
                seqno = null;
                obj.seqNo = null;
            }
            string msg = "";
            tblProduct product = _repositoryWrapper.Product.FindByCondition (x => x.ExhID == exhId && x.p_ID == productId && x.deleteFlag == false).FirstOrDefault ();
            if (product != null) {
                _repositoryWrapper.Product.UpdateProduct (obj, product);
                id = productId;
            } else {
                productId = _repositoryWrapper.SiteRunNumber.GetNewId (SiteRunNumbers.ProductRunKey);
                _repositoryWrapper.Product.SaveNewProduct (obj, exhId, productId);
                _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey (SiteRunNumbers.ProductRunKey);
                id = productId;
            }
            return msg;
        }

        public void DeleteProductByProductId (string exhId, string productId) {
            tblProduct product = _repositoryWrapper.Product.FindByCondition (x => x.ExhID == exhId && x.p_ID == productId && x.deleteFlag == false).FirstOrDefault ();
            if (product != null) {
                _repositoryWrapper.Product.DeleteProduct (product);
            }
        }

        public dynamic GetProductDetailsById (string exhId, string productId) {
            //tblProduct product = _repositoryWrapper.Product.FindByCondition(x => x.ExhID == exhId && x.p_ID == productId && x.deleteFlag == false).FirstOrDefault();
            return _repositoryWrapper.Product.GetProductDetails (exhId, productId);
        }

        public dynamic GetProductInfoById (string productId) {
            tblProduct product = _repositoryWrapper.Product.FindByCondition (x => x.p_ID == productId && x.deleteFlag == false).FirstOrDefault ();
            return product;
        }

        public dynamic GetAllProductList (string exhId, dynamic obj) {
            return _repositoryWrapper.Product.GetProductsByExhId (exhId, obj);
        }

        public dynamic GetAllProductList (string exhId) {
            dynamic result = _repositoryWrapper.Product.FindByCondition (x => x.ExhID == exhId && x.deleteFlag == false).Select (x => new { x.p_ID, x.p_name, x.sororder }).OrderBy (x => x.sororder).ToList ();
            return result;
        }

        public dynamic GetProductCommantListByProductId (string exhId, string productId) {
            return _repositoryWrapper.ProductCommant.GetProductCommantList (exhId, productId);
        }

        public async Task<string> UploadProductFile (IFormFile file, string productId) {
            tblProduct product = _repositoryWrapper.Product.FindByCondition (x => x.p_ID == productId && x.deleteFlag == false).SingleOrDefault ();
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string returnUrl = Configuration.GetSection ("appSettings:SingnalUrl").Value;
            // string url = GlobalFunction.SaveFile(file, product.ExhID, returnUrl);
            string url = await GlobalFunction.SaveFileToS3 (file, product.ExhID, Configuration);
            if (product != null) {
                _repositoryWrapper.Product.updateProductFileUrl (product, url);
            }
            return url;
        }

        public dynamic AddToMyFavourite (string userId, string productId) {
            dynamic result = null;
            tbl_UsersFavourite usersFavourite = _repositoryWrapper.Favourite.FindByCondition (x => x.UserId == userId &&
                x.FavItemId == productId && (x.Status == 0 || x.Status == 1) && x.ItemType == FavouriteItem.Product).SingleOrDefault ();
            if (usersFavourite != null) {
                //Update the existing record
                result = _repositoryWrapper.Product.UpdateFavProductByVisitor (usersFavourite, 1, userId);
            } else {
                //Add new
                result = _repositoryWrapper.Product.addToFavProduct (userId, productId);
            }
            return result;
        }

        public dynamic GetProductViewedCountByExhibitorId (string exhId) {
            return _repositoryWrapper.Product.GetProductViewedCountByExhibitorId (exhId);
        }

        public dynamic RemoveFavProduct (string userId, string productId) {
            dynamic result = null;
            tbl_UsersFavourite usersFavourite = _repositoryWrapper.Favourite.FindByCondition (x => x.UserId == userId &&
                x.FavItemId == productId && x.Status == 1 && x.ItemType == FavouriteItem.Product).FirstOrDefault ();
            if (usersFavourite != null) {
                result = _repositoryWrapper.Product.UpdateFavProductByVisitor (usersFavourite, 0, userId);
            }
            return result;
        }

        public string GetProductURL (string productId) {
            string productUrl = "";
            tblProduct product = _repositoryWrapper.Product.FindByCondition (x => x.p_ID == productId && x.deleteFlag == false).FirstOrDefault ();
            if (product != null) {
                productUrl = product.URL;
            }
            return productUrl;
        }

        public dynamic ViewProductByCountry (string exhId) {
            return _repositoryWrapper.Product.ViewProductByCountry (exhId);
        }
    }
}