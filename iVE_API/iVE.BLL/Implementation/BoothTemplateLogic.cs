using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace iVE.BLL.Implementation {
    public class BoothTemplateLogic : IBoothTemplateLogic {
        private IRepositoryWrapper _repositoryWrapper;
        public BoothTemplateLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
        }

        public dynamic GetAllBoothTemplate () {
            return _repositoryWrapper.BoothTemplate.GetAllBoothTemplate ();
        }

        public dynamic GetBoothTemplateById (string Id) {
            return _repositoryWrapper.BoothTemplate.GetBoothTemplateById (Id);
        }
    }
}