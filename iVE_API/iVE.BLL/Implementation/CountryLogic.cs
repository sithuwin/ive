using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
namespace iVE.BLL.Implementation{
    public class CountryLogic : ICountryLogic{
        private IRepositoryWrapper _repositoryWrapper;
        public CountryLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
        }

        public dynamic GetCountryList () {
            return _repositoryWrapper.Country.GetCountryList();
        }
    }
}