using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using iVE.BLL.ChatAPI;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace iVE.BLL.Implementation {
    public class ChatLogic : IChatLogic {
        private IRepositoryWrapper _repositoryWrapper;
        public ChatLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
        }

        public bool JoinChatWithContactPerson (string exhId, string userId) {
            bool result = false;
            tbl_Account account = _repositoryWrapper.Account.FindByCondition (x => x.a_ID == userId && x.deleteFlag == false).FirstOrDefault ();
            tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition (x => x.ExhID == exhId ).FirstOrDefault ();
            string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
            List<string> contactPersonList = _repositoryWrapper.Account.FindByCondition (x => x.ExhId == exhId && x.deleteFlag == false).Select (x => x.a_ID).ToList ();
            if (account != null && exhibitor != null) {
                string gId = (exhId + "_" + userId).ToLower ();
                string gName = exhibitor.CompanyName;
                bool isOk = GroupAPI.CreateGroup (gId, gName, showPrefix);
                if (isOk) {
                    GroupAPI.JoinMember (userId, gId, UserType.Delegate, showPrefix);
                    foreach (var item in contactPersonList) {
                        GroupAPI.JoinMember (item, gId, UserType.Speaker, showPrefix);
                    }
                    result = true;
                }
            }
            return result;
        }

        
    }
}