using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace iVE.BLL.Implementation
{
    public class AppointmentLogic : IAppointmentLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        public AppointmentLogic(IRepositoryWrapper rw)
        {
            _repositoryWrapper = rw;
        }

        public dynamic CreateTimeSlots(dynamic obj)
        {
            string startTime = obj.startTime;
            string durationString = obj.duration;
            int duration = Convert.ToInt32(durationString);
            string timeSlotPerDayString = obj.timeSlotPerDay;
            int timeSlotPerDay = Convert.ToInt32(timeSlotPerDayString);
            List<string> startTimeList = new List<string>();

            var dayList = _repositoryWrapper.Appointment.GetActiveEventDate();
            int DayCount = dayList.Count;

            DateTime tmpDateTime = DateTime.ParseExact(startTime, "HH:mm:ss", CultureInfo.InvariantCulture);
            DateTime latestDateTime = System.DateTime.Now.AddDays(1);
            TimeSpan ts = new TimeSpan(0, 0, 0);
            latestDateTime = latestDateTime.Date + ts;

            for (int i = 0; i < timeSlotPerDay; i++)
            {
                if (tmpDateTime < latestDateTime)
                {
                    startTime = tmpDateTime.ToString("HH:mm:ss", CultureInfo.InvariantCulture);
                    startTimeList.Add(startTime);
                    tmpDateTime = tmpDateTime.AddMinutes(duration);
                }
            }

            SiteRunNumber siteRunNoObj = _repositoryWrapper.SiteRunNumber.GetRunNumberPrefixAndCurrentNo(SiteRunNumbers.TimeSlotRunKey);

            //Call Repository
            bool isOK = _repositoryWrapper.Appointment.CreateTimeSlots(dayList, startTimeList, duration, siteRunNoObj.prefix, siteRunNoObj.runno_curr, siteRunNoObj.runno_incr);
            int tmpSiteIncreateNo = siteRunNoObj.runno_incr;
            int currentSiteNo = startTime.Count() * DayCount * tmpSiteIncreateNo;
            if (currentSiteNo > 0)
            {
                _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKeyAndCurrentNo(SiteRunNumbers.TimeSlotRunKey, currentSiteNo);
            }
            return isOK;
        }

        public dynamic GetUserSchedule(string ownerId, string userId)
        {
            string currentUser = _repositoryWrapper.Appointment.GetAppointmentUserType(userId);
            string curretnUserType = "";

            if (currentUser == AppointmentUserType.Exhibitor)
            {
                curretnUserType = AppointmentBookingType.ExhibitorBlock;
            }

            if (currentUser == AppointmentUserType.Visitor)
            {
                curretnUserType = AppointmentBookingType.VisitorBlock;
            }

            if (currentUser == AppointmentUserType.Organizer)
            {
                curretnUserType = AppointmentBookingType.OrganizerBlock;
            }
            var objResult = _repositoryWrapper.Appointment.GetUserSchedule(ownerId, userId, curretnUserType);
            return objResult;
        }

        public dynamic GetUserOwnSchedule(string ownerId)
        {
            var objResult = _repositoryWrapper.Appointment.GetUserOwnSchedule(ownerId);
            return objResult;
        }

        public dynamic GetUserScheduleTimeSlot(string userId, string timeSlotId)
        {
            var objResult = _repositoryWrapper.Appointment.GetUserScheduleInfo(userId, timeSlotId);
            return objResult;
        }

        public dynamic GetOwnerTimeSlotRequest(string ownerId, string timeSlotId)
        {
            var objResult = _repositoryWrapper.Appointment.GetOwnerRequest(ownerId, timeSlotId);
            return objResult;
        }

        public dynamic GetOwnerTimeSlotReceivedRequestList(string ownerId, string timeSlotId, dynamic obj)
        {
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "requestDate";
            if (sortBy == null || sortBy == "")
                sortBy = "desc";
            var objResult = _repositoryWrapper.Appointment.GetOwnerReceivedRequestList(ownerId, timeSlotId, obj, sortField, sortBy);
            return objResult;
        }

        public dynamic RequestForAppointment(dynamic obj)
        {
            string userId = obj.requestFrom;
            string currentUser = _repositoryWrapper.Appointment.GetAppointmentUserType(userId);
            bool isOk = _repositoryWrapper.Appointment.CreateRequest(obj, currentUser);
            if (isOk)
            {
                var requestTemplate = _repositoryWrapper.NotificationTemplate.FindByCondition(x => x.TitleMessage == NotificationType.AppointmentRequest).SingleOrDefault();
                string message = requestTemplate.BodyMessage;
                string alternativeId = obj.requestTo;
                StringBuilder emessage = new StringBuilder(message);

                var user = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == userId && x.deleteFlag == false).FirstOrDefault();
                emessage.Replace("<UserName>", user.a_fullname);

                string bodyMessage = emessage.ToString();
                string titleMessage = requestTemplate.TitleMessage;

                _repositoryWrapper.NotificationSendList.SaveNotificationSendList(alternativeId, NotificationStauts.Send, titleMessage, bodyMessage);

            }

            return isOk;
        }

        public dynamic RejectForRequst(dynamic obj)
        {
            dynamic result = false;
            string from = obj.requestFrom;
            string to = obj.requestTo;
            string timeSlotId = obj.timeSlotId;
            var requestQuery = _repositoryWrapper.Appointment.GetActiveRequest(from, to, timeSlotId);
            if (requestQuery != null)
            {
                result = _repositoryWrapper.Appointment.UpdateRequest(RequestCondition.Reject, requestQuery);
            }
            return result;
        }

        public dynamic CancelForRequst(dynamic obj)
        {
            dynamic result = false;
            string from = obj.requestFrom;
            string to = obj.requestTo;
            string timeSlotId = obj.timeSlotId;
            if (to == "" || to == null)
            {
                var requestQuery = _repositoryWrapper.Appointment.GetActiveRequest(from, timeSlotId);
                if (requestQuery != null)
                {
                    result = _repositoryWrapper.Appointment.UpdateRequest(RequestCondition.Cancel, requestQuery);
                }
            }
            else
            {
                var requestQuery = _repositoryWrapper.Appointment.GetActiveRequest(from, to, timeSlotId);
                if (requestQuery != null)
                {
                    result = _repositoryWrapper.Appointment.UpdateRequest(RequestCondition.Cancel, requestQuery);
                }
            }

            return result;
        }

        public dynamic AcceptForRequst(dynamic obj, string tokenUser)
        {
            dynamic result = false;
            string from = obj.requestFrom;
            string to = obj.requestTo;
            string timeSlotId = obj.timeSlotId;
            //Check Has Valid Request
            var requestQuery = _repositoryWrapper.Appointment.GetActiveRequest(from, to, timeSlotId);
            if (requestQuery != null)
            {
                //Check All Paritcipant Free at Requested Time
                bool isFreeForFrom = _repositoryWrapper.Appointment.IsFreeAtRequestedTime(timeSlotId, from);
                bool isFreeForTo = _repositoryWrapper.Appointment.IsFreeAtRequestedTime(timeSlotId, to);

                if (isFreeForFrom && isFreeForTo)
                {
                    //Reject All Appointment
                    bool isRejected = _repositoryWrapper.Appointment.RejectAllReceivedRequest(from, to, timeSlotId);

                    //Change Requst Status IP to COM
                    bool isUpdated = _repositoryWrapper.Appointment.UpdateRequest(RequestCondition.Completed, requestQuery);

                    //Create Appointment
                    string currentUser = _repositoryWrapper.Appointment.GetAppointmentUserType(from);
                    string appointmentBookingType = "";

                    if (currentUser == AppointmentUserType.Exhibitor)
                    {
                        appointmentBookingType = AppointmentBookingType.ExhibitorRequst;
                    }

                    if (currentUser == AppointmentUserType.Visitor)
                    {
                        appointmentBookingType = AppointmentBookingType.VisterRequest;
                    }

                    string appointmentId = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.AppointmentRunKey);
                    bool isAppointedCreated = _repositoryWrapper.Appointment.CreateAppointment(appointmentId, timeSlotId, AppointmentCondition.AppointmentConfirmed, appointmentBookingType, tokenUser, "");
                    _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.AppointmentRunKey);

                    //Add to Participant My Schedule
                    bool fromUserOkay = _repositoryWrapper.Appointment.CreateMySchedule(appointmentId, from);
                    bool toUserOkay = _repositoryWrapper.Appointment.CreateMySchedule(appointmentId, to);

                    if (isRejected && isUpdated && isAppointedCreated && fromUserOkay && toUserOkay)
                    {
                        result = true;
                    }
                }
            }

            if (result)
            {
                var requestTemplate = _repositoryWrapper.NotificationTemplate.FindByCondition(x => x.TitleMessage == NotificationType.AppointmentConfirm).SingleOrDefault();
                string message = requestTemplate.BodyMessage;
                StringBuilder emessage = new StringBuilder(message);

                var user = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == to && x.deleteFlag == false).FirstOrDefault();
                emessage.Replace("<UserName>", user.a_fullname);

                string bodyMessage = emessage.ToString();
                string titleMessage = requestTemplate.TitleMessage;

                _repositoryWrapper.NotificationSendList.SaveNotificationSendList(from, NotificationStauts.Send, titleMessage, bodyMessage);
            }
            return result;
        }

        public dynamic CancelForAppointment(dynamic obj)
        {
            dynamic result = false;
            string userId = obj.userId;
            string timeSlotId = obj.timeSlotId;
            var appQuery = _repositoryWrapper.Appointment.GetActiveAppointment(timeSlotId, userId);
            if (appQuery != null)
            {
                //Access Right to Change ( Org only allowed to edit Pre-shcudel )
                if (appQuery.BookingType != AppointmentBookingType.PreScheduled)
                {
                    bool isOKForParticipants = _repositoryWrapper.Appointment.UpdateAllMyScheduleByAppointmentId(appQuery.AppointmentID);
                    bool isOKForApp = _repositoryWrapper.Appointment.UpdateAppointment(appQuery);
                    if (isOKForParticipants && isOKForApp)
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public dynamic MakeBlockForTimeSlot(dynamic obj, string tokenUser)
        {
            dynamic result = false;
            string userId = obj.userId;
            string timeSlotId = obj.timeSlotId;
            string description = obj.description;

            //Check Appointments Exist
            bool isFree = _repositoryWrapper.Appointment.IsFreeAtRequestedTime(timeSlotId, userId);
            if (isFree)
            {
                //Create Appointment
                string currentUser = _repositoryWrapper.Appointment.GetAppointmentUserType(userId);
                string appointmentBookingType = "";

                if (currentUser == AppointmentUserType.Exhibitor)
                {
                    appointmentBookingType = AppointmentBookingType.ExhibitorBlock;
                }

                if (currentUser == AppointmentUserType.Visitor)
                {
                    appointmentBookingType = AppointmentBookingType.VisitorBlock;
                }

                if (currentUser == AppointmentUserType.Organizer)
                {
                    appointmentBookingType = AppointmentBookingType.OrganizerBlock;
                }

                string appointmentId = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.AppointmentRunKey);
                bool isAppCreated = _repositoryWrapper.Appointment.CreateAppointment(appointmentId, timeSlotId, AppointmentCondition.BlockUser, appointmentBookingType, tokenUser, description);
                _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.AppointmentRunKey);

                //Add to Participant My Schedule
                bool isUserOkay = _repositoryWrapper.Appointment.CreateMySchedule(appointmentId, userId);
                if (isAppCreated && isUserOkay)
                {
                    result = true;
                }
            }
            return result;
        }

        public dynamic CanelBlockForTimeSlot(dynamic obj, string tokenUser)
        {
            dynamic result = false;
            string userId = obj.userId;
            string timeSlotId = obj.timeSlotId;

            //Check Appointments Exist
            var appQuery = _repositoryWrapper.Appointment.GetActiveAppointment(timeSlotId, userId);
            if (appQuery != null)
            {
                //Create Appointment
                string currentUser = _repositoryWrapper.Appointment.GetAppointmentUserType(userId);
                string appointmentBookingType = "";

                if (currentUser == AppointmentUserType.Exhibitor)
                {
                    appointmentBookingType = AppointmentBookingType.ExhibitorBlock;
                }

                if (currentUser == AppointmentUserType.Visitor)
                {
                    appointmentBookingType = AppointmentBookingType.VisitorBlock;
                }

                if (currentUser == AppointmentUserType.Organizer)
                {
                    appointmentBookingType = AppointmentBookingType.OrganizerBlock;
                }

                if (appointmentBookingType == appQuery.BookingType && appQuery.CreatedBy == tokenUser)
                {
                    bool isOKForParticipants = _repositoryWrapper.Appointment.UpdateAllMyScheduleByAppointmentId(appQuery.AppointmentID);
                    bool isOKForApp = _repositoryWrapper.Appointment.UpdateAppointment(appQuery);
                    if (isOKForParticipants && isOKForApp)
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public dynamic GetTimeSlotDetail(string id){
            return _repositoryWrapper.Appointment.GetTimeSlotDetail(id);
        }

    }
}