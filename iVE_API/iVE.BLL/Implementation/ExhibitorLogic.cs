using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using iVE.BLL.ChatAPI;
using iVE.BLL.Interface;
using iVE.DAL;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
namespace iVE.BLL.Implementation {
    public class ExhibitorLogic : IExhibitorLogic {
        private IRepositoryWrapper _repositoryWrapper;
        private IContactPersonLogic _contactPersonLogic;
        public ExhibitorLogic (IRepositoryWrapper rw, IContactPersonLogic contactPersonLogic) {
            _repositoryWrapper = rw;
            _contactPersonLogic = contactPersonLogic;
        }

        public dynamic GetAllExhibitionList (dynamic obj) {
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0) {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "SequenceNo";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";
            var objResult = _repositoryWrapper.Exhibitor.GetAllExhibitionList (obj, sortField, sortBy);
            return objResult;
        }

        public dynamic GetAllExhibitionListForOrganization (dynamic obj) {
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0) {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "sequenceNo";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";
            var objResult = _repositoryWrapper.Exhibitor.GetAllExhibitionListForOrg (obj, sortField, sortBy);
            return objResult;
        }

        public dynamic GetExhibitorById (string Id) {
            return _repositoryWrapper.Exhibitor.GetExhibitorData (Id);
        }

        public dynamic GetVisitedVisitor (dynamic obj, string exhId, string status, string returnUrl, bool isExport) {
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0) {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "fullName";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";
            var exhibitor = _repositoryWrapper.Exhibitor.GetVisitedVisitor (obj, sortField, sortBy, exhId, status, returnUrl, isExport);
            return exhibitor;
        }

        public void UpdateExhibitorById (dynamic obj, string exhId) {
            tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition (x => x.ExhID == exhId).FirstOrDefault ();
            if (exhibitor != null) {
                _repositoryWrapper.Exhibitor.UpdateExhibitor (obj, exhibitor);
            }
        }

        public string CreateOrUpdateExhibior (dynamic obj, string exhId, ref string insertedNewExhID) {
            string boothNo = obj.boothNo;
            string SequenceNo = obj.sequenceNo;
            string hallNo = obj.hallNo;
            string message = "";

            var boothObj = _repositoryWrapper.Exhibitor
                .FindByCondition (x => (exhId == "0" || x.ExhID != exhId) && x.HallNo == hallNo && x.BoothNo == boothNo && boothNo != null)
                .Select (x => x.BoothNo).FirstOrDefault ();

            var seqObj = _repositoryWrapper.Exhibitor
                .FindByCondition (x => (exhId == "0" || x.ExhID != exhId) && SequenceNo != null && x.SequenceNo.ToString () == SequenceNo)
                .Select (x => x.SequenceNo).FirstOrDefault ();
            if (boothObj != null) {
                message = "Duplicate booth No.";
            } else if (seqObj != null) {
                message = "Duplicate sequence No.";
            } else {
                tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition (x => x.ExhID == exhId).FirstOrDefault ();
                if (exhibitor != null) {
                    _repositoryWrapper.Exhibitor.UpdateExhibitor (obj, exhibitor);
                } else {
                    exhId = _repositoryWrapper.SiteRunNumber.GetNewId (SiteRunNumbers.ExhibitorRunKey);
                    _repositoryWrapper.Exhibitor.CreateExhibitor (obj, exhId);
                    _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey (SiteRunNumbers.ExhibitorRunKey);
                    insertedNewExhID = exhId;
                }
            }
            return message;
        }

        public string CreateOrUpdateExhibiorOrg (dynamic obj, string exhId, ref string insertedNewExhID) {
            string boothNo = obj.boothNo;
            string SequenceNo = obj.sequenceNo;
            string hallNo = obj.hallNo;
            string message = "";

            var boothObj = _repositoryWrapper.Exhibitor
                .FindByCondition (x => (exhId == "0" || x.ExhID != exhId) && x.HallNo == hallNo && x.BoothNo == boothNo && boothNo != null)
                .Select (x => x.BoothNo).FirstOrDefault ();

            var seqObj = _repositoryWrapper.Exhibitor
                .FindByCondition (x => (exhId == "0" || x.ExhID != exhId) && SequenceNo != null && x.SequenceNo.ToString () == SequenceNo)
                .Select (x => x.SequenceNo).FirstOrDefault ();
            if (boothObj != null) {
                message = "Duplicate booth No.";
            } else if (seqObj != null) {
                message = "Duplicate sequence No.";
            } else {
                tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition (x => x.ExhID == exhId).FirstOrDefault ();
                if (exhibitor != null) {
                    _repositoryWrapper.Exhibitor.UpdateExhibitorOrg (obj, exhibitor);
                } else {
                    exhId = _repositoryWrapper.SiteRunNumber.GetNewId (SiteRunNumbers.ExhibitorRunKey);
                    _repositoryWrapper.Exhibitor.CreateExhibitor (obj, exhId);
                    _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey (SiteRunNumbers.ExhibitorRunKey);
                    insertedNewExhID = exhId;
                }
            }
            return message;
        }

        public async Task<string> UploadFile (IFormFile file, string exhId) {
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string returnUrl = Configuration.GetSection ("appSettings:SingnalUrl").Value;
            // string url = GlobalFunction.SaveFile (file, exhId, returnUrl);
            string url = await GlobalFunction.SaveFileToS3 (file, exhId, Configuration);
            return url;
        }

        public void UpdateExhibitorStatus (string exhId, string status) {
            if (ExhibitorStatus.Approved == status || ExhibitorStatus.Submitted == status || ExhibitorStatus.Pending == status ||
                ExhibitorStatus.Published == status) {
                tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition (x => x.ExhID == exhId).FirstOrDefault ();
                if (exhibitor != null) {
                    _repositoryWrapper.Exhibitor.UpdateExhibitorStatusByExhibitorId (exhId, status, exhibitor);
                }
            }

        }

        #region ChatContactGroupMember (8-7-2020 th)
        public void AddChatContactMember (string exhId, string userId) {
            tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition (x => x.ExhID == exhId).FirstOrDefault ();
            if (exhibitor != null) {
                _repositoryWrapper.Exhibitor.AddChatContactMember (exhId, userId, exhibitor);
                AddContactPersonToExistingChat (exhId, userId);
            }
        }
        // adding new contact person to existing chat group 
        private void AddContactPersonToExistingChat (string exhId, string userId) {
            string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowPrefix).Select (x => x.Value).FirstOrDefault ();
            // getting Existing contantperson's Id (group chat )
            string groupMemberUserId = _repositoryWrapper.Exhibitor.GetExistingGroupMemberId (exhId);

            //if there is a group 
            if (groupMemberUserId != null && groupMemberUserId != "") {
                dynamic data = GroupAPI.GetExistingGroup (exhId, showPrefix, groupMemberUserId);
                string guid = "";
                for (int i = 0; i < data.data.Count; i++) {
                    guid = data.data[i].guid;
                    guid = guid.Split ("_") [1] + "_" + guid.Split ("_") [2];
                    bool GroupExist = CheckGroupExistOrNot (guid, showPrefix);
                    if (GroupExist) {
                        GroupAPI.JoinMember (userId, guid, UserType.Speaker, showPrefix);
                    }
                }
            }
        }
        //checking group exist or not 
        private bool CheckGroupExistOrNot (string guid, string showPrefix) {
            bool result = false;
            dynamic data = GroupAPI.GetGroup (guid, showPrefix);
            if (data != null) {
                result = true;
            }
            return result;

        }
        public void RemoveChatContactMember (string exhId, string userId) {
            tblExhibitor exhibitor = _repositoryWrapper.Exhibitor.FindByCondition (x => x.ExhID == exhId).FirstOrDefault ();
            if (exhibitor != null) {
                _repositoryWrapper.Exhibitor.RemoveChatContactMember (exhId, userId, exhibitor);
                RemoveContactPersonFromExistingChat (exhId, userId);
            }
        }
        // Removing ContactPerson from existing chat 
        private void RemoveContactPersonFromExistingChat (string exhId, string userId) {
            string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowPrefix).Select (x => x.Value).FirstOrDefault ();

            dynamic data = GroupAPI.GetExistingGroup (exhId, showPrefix, userId);
            string guid = "";
            for (int i = 0; i < data.data.Count; i++) {
                guid = data.data[i].guid;
                guid = guid.Split ("_") [1] + "_" + guid.Split ("_") [2];
                bool GroupExist = CheckGroupExistOrNot (guid, showPrefix);
                if (GroupExist) {
                    GroupAPI.RemoveMemberFromGroup (userId, guid, showPrefix);
                }

            }
        }

        public dynamic GetChatContactMembers (string exhId) {
            return _repositoryWrapper.Exhibitor.GetChatContactMembers (exhId);
        }

        public dynamic GetHallList () {
            return _repositoryWrapper.Exhibitor.GetHallList ();
        }

        public dynamic GetExhibitorTypeList () {
            return _repositoryWrapper.Exhibitor.GetExhibitorTypeList ();
        }

        public dynamic GetBoothList () {
            return _repositoryWrapper.Exhibitor.GetBoothList ();
        }

        public dynamic GetExhibitorCategoryList () {
            return _repositoryWrapper.Exhibitor.GetExhibitorCategory ();
        }

        public dynamic GetExhibitorList (dynamic obj) {
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0) {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "SequenceNo";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";
            return _repositoryWrapper.Exhibitor.GetExhibitors (obj, sortField, sortBy);
        }

        // public dynamic GetChatContactMembers(string exhId)
        // {
        //     return _repositoryWrapper.Exhibitor.GetChatContactMembers(exhId);
        // }

        public dynamic ExhibitorRecommendationReport (dynamic obj, bool isExport) {
            return _repositoryWrapper.Exhibitor.ExhibitorRecommendationReport (obj, isExport);
        }

        public dynamic VisitedAnalysisByCountry (string exhId) {
            return _repositoryWrapper.Exhibitor.VisitedAnalysisByCountry (exhId);
        }

        public dynamic VisitedAnalysisByDay (string exhId) {
            return _repositoryWrapper.Exhibitor.VisitedAnalysisByDay (exhId);
        }

        public dynamic VisitedAnalysis (string exhId) {
            return _repositoryWrapper.Exhibitor.VisitedAnalysis (exhId);
        }
        public dynamic ViewedExhProductByCountry (string exhId) {
            return _repositoryWrapper.Exhibitor.ViewedExhProductByCountry (exhId);
        }

        public dynamic FavouriteExhProduct (string exhId) {
            return _repositoryWrapper.Exhibitor.FavouriteExhProduct (exhId);
        }

        public dynamic GetExhibitorDashboardStatus (string exhId) {
            return _repositoryWrapper.Exhibitor.GetExhibitorDashboardStatus (exhId);
        }

        public dynamic GetVisitedVisitorList (dynamic obj, bool isExport) {
            return _repositoryWrapper.Exhibitor.GetVisitedVisitorList (obj, isExport);
        }

        public dynamic GetVisitedVisitorCountryList (dynamic obj, bool isExport) {
            return _repositoryWrapper.Exhibitor.GetVisitedVisitorCountryList (obj, isExport);
        }

        public dynamic VisitorLoginAnalysisByCountry (dynamic obj, bool isExport) {
            return _repositoryWrapper.Exhibitor.VisitorLoginAnalysisByCountry (obj, isExport);
        }

        public List<string> GetCountryList () {
            return _repositoryWrapper.Exhibitor.GetCountryList ();
        }
        public List<string> GetDayList () {
            return _repositoryWrapper.Exhibitor.GetDayList ();
        }

        public dynamic GetExhibitorsByCountry () {
            return _repositoryWrapper.Exhibitor.GetExhibitorsByCountry ();
        }

        public dynamic GetExhibitorsStatus () {
            return _repositoryWrapper.Exhibitor.GetExhibitorsStatus ();
        }
        public dynamic GetExhBoothTemplates () {
            return _repositoryWrapper.Exhibitor.GetExhBoothTemplates ();
        }
        #endregion

        #region Exhibitor Mass Upload
        public dynamic SaveExhibitorUpload (DataTable dt, ref string msg) {
            bool result = false;
            msg = "";

            try {
                if (dt.Rows.Count <= 0) {
                    msg = "Data is null";
                } else {
                    dt.Columns.Add ("UploadStatus");
                    foreach (DataRow dr in dt.Rows) {
                        dynamic obj = null;
                        obj = getExhObjFromDataRow (dr, UserType.Exhibitor);
                        if (obj != null) {
                            if (!string.IsNullOrEmpty (obj.companyName)) {
                                string exhId = "";
                                string messageSaveExhibitor = CreateOrUpdateExhibior (obj, exhId, ref exhId);
                                if (string.IsNullOrEmpty (messageSaveExhibitor) && !string.IsNullOrEmpty (exhId)) {
                                    obj = null;
                                    obj = getExhObjFromDataRow (dr, UserType.MainExhibitor);
                                    if (obj != null) {
                                        if (!string.IsNullOrEmpty (obj.name) && !string.IsNullOrEmpty (obj.email) &&
                                            !string.IsNullOrEmpty (obj.loginName) && !string.IsNullOrEmpty (obj.password)) {
                                            string ContactID = "";
                                            string messageSaveContact = _contactPersonLogic.AddOrUpdateContactPerson (obj, exhId, ContactID, ref ContactID);
                                            if (string.IsNullOrEmpty (messageSaveContact)) {
                                                dr["UploadStatus"] = "Successfully Created";
                                                result = true;
                                            } else {
                                                dr["UploadStatus"] = messageSaveContact;
                                            }
                                        } else {
                                            dr["UploadStatus"] = "No Complete Contact Data";
                                        }
                                    } else {
                                        dr["UploadStatus"] = "No Contact Person Data";
                                    }
                                } else {
                                    dr["UploadStatus"] = messageSaveExhibitor;
                                }
                            } else {
                                dr["UploadStatus"] = "No Complete Company Data";
                            }
                        } else {
                            dr["UploadStatus"] = "No Company Data";
                        }
                    }

                    if (result) {
                        msg = "Exhibitor list(s) are saved successfully";
                    }
                }
            } catch (Exception ex) { }

            object objData = dt;
            dynamic objResponse = objData;
            return objResponse;
        }
        private dynamic getExhObjFromDataRow (DataRow dr, string type) {
            dynamic obj = new object ();
            try {
                if (type == UserType.Exhibitor) {
                    ExhObjCreate exhObj = new ExhObjCreate ();
                    string companyName = dr["CompanyName"] != DBNull.Value ? dr["CompanyName"].ToString () : "";
                    string description = dr["Description"] != DBNull.Value ? dr["Description"].ToString () : "";
                    string country = dr["Country"] != DBNull.Value ? dr["Country"].ToString () : "";
                    if (!string.IsNullOrEmpty (country)) {
                        country = _repositoryWrapper.Country.GetCountryISO3DigitByCountry (country);
                    }
                    string exhType = dr["ExhType"] != DBNull.Value ? dr["ExhType"].ToString () : "";
                    if (!string.IsNullOrEmpty (exhType)) {
                        exhType = _repositoryWrapper.Exhibitor.GetExhibitorTypeIDByType (exhType);
                    }
                    string hallNo = dr["HallNo"] != DBNull.Value ? dr["HallNo"].ToString () : "";
                    string boothNo = dr["BoothNo"] != DBNull.Value ? dr["BoothNo"].ToString () : "";
                    string sequenceNo = dr["SequenceNo"] != DBNull.Value ? dr["SequenceNo"].ToString () : "";

                    exhObj.companyName = companyName;
                    exhObj.description = description;
                    exhObj.country = country;
                    exhObj.exhType = exhType;
                    exhObj.hallNo = hallNo;
                    exhObj.boothNo = boothNo;
                    exhObj.sequenceNo = sequenceNo;
                    obj = exhObj;
                } else if (type == UserType.MainExhibitor) {
                    ContactObjCreate contactObj = new ContactObjCreate ();
                    string contactName = dr["ContactPersonName"] != DBNull.Value ? dr["ContactPersonName"].ToString () : "";
                    string address = dr["ContactPersonAddress"] != DBNull.Value ? dr["ContactPersonAddress"].ToString () : "";
                    string email = dr["ContactPersonEmail"] != DBNull.Value ? dr["ContactPersonEmail"].ToString () : "";
                    string tel = dr["ContactPersonTelephone"] != DBNull.Value ? dr["ContactPersonTelephone"].ToString () : "";
                    string fax = dr["ContactPersonFax"] != DBNull.Value ? dr["ContactPersonFax"].ToString () : "";
                    string mobile = dr["ContactPersonMobile"] != DBNull.Value ? dr["ContactPersonMobile"].ToString () : "";
                    string loginName = dr["ContactPersonLoginName"] != DBNull.Value ? dr["ContactPersonLoginName"].ToString () : "";
                    string password = dr["ContactPersonPassword"] != DBNull.Value ? dr["ContactPersonPassword"].ToString () : "";

                    contactObj.type = type;
                    contactObj.name = contactName;
                    contactObj.address = address;
                    contactObj.email = email;
                    contactObj.tel = tel;
                    contactObj.fax = fax;
                    contactObj.mobile = mobile;
                    contactObj.loginName = loginName;
                    contactObj.password = password;
                    obj = contactObj;
                }
            } catch (Exception ex) { }

            return obj;
        }
        public class ExhObjCreate {
            public string companyName { get; set; }
            public string description { get; set; }
            public string country { get; set; }
            public string exhType { get; set; }
            public string hallNo { get; set; }
            public string boothNo { get; set; }
            public string sequenceNo { get; set; }

            public string exhCategory { get; set; } = "";
            public string boothTemplate { get; set; } = "";
            public string logo { get; set; } = "";
            public string boothBanner { get; set; } = "";
            public string companyVideo { get; set; } = "";
            public string companyDocument { get; set; } = "";

            public string facebook { get; set; } = "";
            public string instragram { get; set; } = "";
            public string twitter { get; set; } = "";
            public string linkin { get; set; } = "";
        }
        public class ContactObjCreate {
            public string type { get; set; }
            public string name { get; set; }
            public string address { get; set; }
            public string email { get; set; }
            public string tel { get; set; }
            public string fax { get; set; }
            public string mobile { get; set; }
            public string loginName { get; set; }
            public string password { get; set; }
        }
        #endregion

        public string GetNotificationData (string userName) {
            return _repositoryWrapper.NotificationTemplate.GetNotificationData (userName);
        }

        public dynamic GetDailyActiveUsers (dynamic obj) {
            return _repositoryWrapper.Exhibitor.GetDailyActiveUsers (obj);
        }
        public dynamic ExhEngagementReportForOrg (dynamic obj, bool isExport) {
            return _repositoryWrapper.Exhibitor.ExhEngagementDetails (obj, isExport);
        }

        public dynamic ActivitySummaryReport (dynamic obj) {
            return _repositoryWrapper.Exhibitor.ActivitySummaryReport (obj);
        }

        public bool ChangeExhibitorChatStatus (string userId, int type) {
            bool result = false;
            tbl_Account exhDetail = _repositoryWrapper.Account.FindByCondition (x => x.a_ID == userId && x.deleteFlag == false).FirstOrDefault ();
            if (exhDetail != null) {
                exhDetail.ChatActiveStatus = type;
                _repositoryWrapper.Account.Update (exhDetail);
                _repositoryWrapper.Save ();
                result = true;
            }
            return result;
        }
    }
}