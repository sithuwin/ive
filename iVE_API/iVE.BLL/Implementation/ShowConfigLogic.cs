using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
namespace iVE.BLL.Implementation {
    public class ShowConfigLogic : IShowConfigLogic {
        private IRepositoryWrapper _repositoryWrapper;
        public ShowConfigLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
        }

        public dynamic GetShowConfig () {
            return _repositoryWrapper.ShowConfig.GetShowConfig ();
        }

        public string CreateOrUpdateShowConfig (dynamic obj, string name) {
            tblShowConfig showconfig = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == name && x.Status == 1).FirstOrDefault ();
            if (showconfig != null) {
                _repositoryWrapper.ShowConfig.UpdateShowConfig (obj, showconfig);
            } else {
                _repositoryWrapper.ShowConfig.SaveNewShowConfig (obj, name);
            }
            return name;
        }

        public void DeleteShowConfigByName (string name) {
            tblShowConfig showconfig = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == name && x.Status == 1).FirstOrDefault ();
            if (showconfig != null) {
                _repositoryWrapper.ShowConfig.DeleteShowConfig (showconfig);
            }
        }

    }
}