using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
namespace iVE.BLL.Implementation {
    public class FavouriteLogic : IFavouriteLogic {
        private IRepositoryWrapper _repositoryWrapper;
        public FavouriteLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
        }

        public dynamic GetFavouriteExhibitors (string userid) {
            return _repositoryWrapper.Favourite.GetFavouriteExhibitors (userid);
        }

        public void UpdateUsersFavourite(string actionId, string favouriteId,string favItemType)
        {
            tbl_UsersFavourite obj = _repositoryWrapper.Favourite.FindByCondition(x => x.ItemType == favItemType
            && x.UserId == actionId && x.FavItemId == favouriteId).FirstOrDefault();
            if (obj == null)
            {
                //Add favourite
                _repositoryWrapper.Favourite.SaveNewUserFavourite(actionId, favouriteId, favItemType);
            }
            else
            {
                //Update based on the existing favourite record
                if (obj.Status == 0)
                {
                    _repositoryWrapper.Favourite.UpdateUserFavourite(obj, 1);
                }
                else
                {
                    _repositoryWrapper.Favourite.UpdateUserFavourite(obj, 0);
                }
            }
        }
       
    }
}