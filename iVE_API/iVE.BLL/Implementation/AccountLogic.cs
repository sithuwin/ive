using System.Linq;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
namespace iVE.BLL.Implementation
{
    public class AccountLogic : IAccountLogic
    {
        private IRepositoryWrapper _repositoryWrapper;
        public AccountLogic (IRepositoryWrapper rw) {
            _repositoryWrapper = rw;
        }

        public dynamic GetVisitor(dynamic obj){
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            bool isDefaultSort = false;
            if (sortField == null || sortField == "")
                sortField = "fullName";
            isDefaultSort = true;
            if (sortBy == null || sortBy == "")
                sortBy = "asc";
            var visitor = _repositoryWrapper.Account.GetVisitor(obj, sortField, sortBy, isDefaultSort);
            return visitor;
        }

        public dynamic GetVisitorById(string Id){
            var visitor = _repositoryWrapper.Account.GetVisitorById (Id);
            return visitor;
        }

        public dynamic GetVisitedExhListByVisitor(string Id){
            var visitor = _repositoryWrapper.Account.GetVisitedExhListByVisitorId(Id);
            return visitor;
        }

        public string CreateOrUpdateVisitor(dynamic obj, string visitorId)
        {
            string result = "";
            string email = obj.email;
            string userName = obj.loginName;

            var emailObj = _repositoryWrapper.Account
            .FindByCondition(x => x.deleteFlag == false && x.a_email.ToLower() == email.ToLower() && (visitorId == "0" || x.a_ID != visitorId))
            .Select(x => x.a_email).FirstOrDefault();
            var userNameObj = _repositoryWrapper.Account
            .FindByCondition(x => x.deleteFlag == false && x.a_LoginName.ToLower() == userName.ToLower() && (visitorId == "0" || x.a_ID != visitorId))
            .Select(x => x.a_LoginName).FirstOrDefault();
            if (emailObj != null)
            {
                result = "Email already existed!";
            }
            else if (userNameObj != null)
            {
                result = "Login name already existed!";
            }
            else
            {
                tbl_Account acountObj = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == visitorId && x.deleteFlag == false).FirstOrDefault();
                if (acountObj != null)
                {
                    _repositoryWrapper.Account.UpdateVisitor(obj, acountObj);
                }
                else
                {
                    visitorId = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.AccountRunKey);
                    _repositoryWrapper.Account.SaveNewVisitor(obj, visitorId);
                    _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.AccountRunKey);
                }
            }
            return result;
        }

        public void DeleteVisitorByVisitorId(string visitorId)
        {
            tbl_Account acountObj = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == visitorId && x.deleteFlag == false).FirstOrDefault();
            if (acountObj != null)
            {
                _repositoryWrapper.Account.DeleteVisitor(acountObj);
            }
        }

        public dynamic GetFavouriteExhibitorsByVisitorId(string Id){
            return _repositoryWrapper.Account.GetFavouriteExhibitorsByVisitorId(Id);
        }

        public dynamic GetFavouriteProductsByVisitorId(string Id){
            return _repositoryWrapper.Account.GetFavouriteProductsByVisitorId(Id);
        }
        
        #region VisitedExhibitor
        public dynamic GetVisitedExhibitor(dynamic obj, string userId)
        {
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "CompanyName";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";
            var objResult = _repositoryWrapper.Account.GetVisitedExhibitor(obj, sortField, sortBy, userId);
            return objResult;
        }
        #endregion

         public void AddUsersFavouriteByVisitor(string vId, string exhId){
            tbl_UsersFavourite obj = _repositoryWrapper.Favourite.FindByCondition(x => x.ItemType == FavouriteItem.Exhibitor
            && (x.Status == 1 || x.Status == 0) && x.UserId == vId && x.FavItemId == exhId).FirstOrDefault();
            if(obj == null){
                //Add a new visitor favourite to exhibitor
                _repositoryWrapper.Favourite.SaveNewUserFavourite(vId,exhId,FavouriteItem.Exhibitor);
            }else
            {
                //Update based on the existing visitor favourite status
                obj = _repositoryWrapper.Favourite.FindByCondition(x => x.Status == 0 && 
                x.ItemType == FavouriteItem.Exhibitor && x.UserId == vId && x.FavItemId == exhId).FirstOrDefault();
                if(obj.Status == 0){
                    _repositoryWrapper.Favourite.UpdateUserFavourite(obj,1);
                }
            } 
        }

        public void RemoveUsersFavourite(string VId, string exhId){
            tbl_UsersFavourite obj = _repositoryWrapper.Favourite.FindByCondition(x => x.Status == 1 && 
                x.ItemType == FavouriteItem.Exhibitor && x.UserId == VId && x.FavItemId == exhId ).FirstOrDefault();
            if(obj != null){
                _repositoryWrapper.Favourite.UpdateUserFavourite(obj,0);
            }
        }

        public dynamic GetAllExhibitorProductsByVisitorId(dynamic obj,string exhId,string vId)
        {
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            bool isDefaultSort = false;
            if (sortField == null || sortField == "")
                sortField = "p_name";
            isDefaultSort = true;
            if (sortBy == null || sortBy == "")
                sortBy = "asc";

            var objResult = _repositoryWrapper.Account.GetAllExhibitorProductsByVisitorId(obj, sortField, sortBy, isDefaultSort, exhId, vId);
            return objResult;
        }

        public dynamic GetFavouriteExhStatusByVisitor(string vId, string exhId){
            var result = _repositoryWrapper.Account.GetFavouriteExhStatusByVisitor(vId,exhId);
            return result;
        }

        public dynamic GetVisitorByCountry(){
            return _repositoryWrapper.Account.GetVisitorsByCountry();
        }

         public dynamic GetActiveLoginUsers(dynamic obj,bool isexport){
            return _repositoryWrapper.Account.GetActiveLoginUsers(obj,isexport);
        }

        public dynamic GetDays()
        {
            return _repositoryWrapper.Account.GetDays();
        }
    }
}