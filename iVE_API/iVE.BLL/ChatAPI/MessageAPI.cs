using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using iVE.DAL.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
namespace iVE.API.ChatAPI
{
    public static class MessageAPI
    {
        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler
        {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        public static dynamic GetMessages(int limit, string receiverType, string sendAt, string id)
        {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            string appId = Configuration.GetSection("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection("ChatKey:region").Value;
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("appId", appId);
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                string category = "message";
                string affix = "prepend";

                string metaData = "";
                if (!String.IsNullOrEmpty(id))
                {
                    metaData = "&id=" + id + "&affix=" + affix;
                }

                string url = "https://api-" + regionKey + ".cometchat.io/v2.0/messages?limit=" + limit + "&category=" + category + "&receiverType=" + receiverType + "&sentAt=" + sendAt + metaData;
                HttpResponseMessage response = client.GetAsync(url).Result;

                var responseContent = response.Content.ReadAsStringAsync().Result;
                dynamic responseJson = JsonConvert.DeserializeObject(responseContent);
                if (response.IsSuccessStatusCode == true)
                {
                    return responseJson;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
            return result;
        }

    }
}