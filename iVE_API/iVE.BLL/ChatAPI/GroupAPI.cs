using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using iVE.DAL.Models;
using iVE.DAL.Util;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
namespace iVE.BLL.ChatAPI {
    public static class GroupAPI {
        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        //private static readonly HttpClient client;
        public static dynamic CreateGroup (string guId, string groupName, string showPrefix) {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            // GlobalFunction.WriteSystemLog("GroupAPI  -   CreateGroup - " + "Creating  Group in Chat");
            try {
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);

                ChatGroup groupModel = new ChatGroup ();
                groupModel.guid = showPrefix + "_" + guId;
                groupModel.name = groupName;
                if (groupName.Length > 100) {
                    groupModel.name = groupName.Substring (0, 100);
                }
                groupModel.type = "private";
                groupModel.description = groupName;

                var content = new StringContent (JsonConvert.SerializeObject (groupModel), Encoding.UTF8, "application/json");
                string url = "https://api-" + regionKey + ".cometchat.io/v2.0/groups";
                HttpResponseMessage response = client.PostAsync (url, content).Result;
                // GlobalFunction.WriteSystemLog("GroupAPI  -   CreateGroup - " + response);
                var responseContent = response.Content.ReadAsStringAsync ().Result;
                dynamic responseJson = JsonConvert.DeserializeObject (responseContent);
                if (response.IsSuccessStatusCode == true) {
                    if (responseJson.data != null) {
                        result = true;
                    }
                } else {
                    if (responseJson.error.details.guid != null) {
                        string alreadyMessage = responseJson.error.details.guid[0].ToString ();
                        if (alreadyMessage == "The guid has already been taken.")
                            result = true;
                    }
                }
                // GlobalFunction.WriteSystemLog("GroupAPI  -   CreateGroup - " + responseContent.ToString());
            } catch (Exception ex) {
                Console.WriteLine (ex.Message.ToString ());
            }
            return result;
        }

        public static dynamic JoinMember (string userId, string guId, string userType, string showPrefix) {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            // GlobalFunction.WriteSystemLog ("GroupAPI  -   JoinMember - " + "Adding member to group in Chat");
            try {
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);

                string[] userList = new string[1];
                userList[0] = showPrefix + "_" + userId;

                userId = (showPrefix + "_" + userId).ToLower ();
                guId = (showPrefix + "_" + guId).ToLower ();
                dynamic members = new GroupParticipants ();
                if (userType == UserType.Organizer) {
                    members = new GroupAdmins ();
                    members.admins = userList;

                } else if (userType == UserType.Speaker) {
                    members = new GroupModerators ();
                    members.moderators = userList;
                } else {
                    members.participants = userList;
                }

                var content = new StringContent (JsonConvert.SerializeObject (members), Encoding.UTF8, "application/json");
                string url = "https://api-" + regionKey + ".cometchat.io/v2.0/groups/" + guId + "/members";
                HttpResponseMessage response = client.PostAsync (url, content).Result;
                //  GlobalFunction.WriteSystemLog ("GroupAPI  -   JoinMember - " + response);
                if (response.IsSuccessStatusCode == true) {
                    var responseContent = response.Content.ReadAsStringAsync ().Result;
                    dynamic responseJson = JsonConvert.DeserializeObject (responseContent);
                    if (responseJson.data != null) {
                        result = true;
                    }
                    // GlobalFunction.WriteSystemLog ("Group API - Add Member " + responseJson.ToString ());
                    // GlobalFunction.WriteSystemLog ("Group API - Member " + members);
                }
            } catch (Exception ex) {
                // GlobalFunction.WriteSystemLog ("Group API - Add Member " + ex.Message.ToString ());
                Console.WriteLine (ex.Message.ToString ());
            }
            return result;
        }

        public static dynamic GetGroup (string guId, string showPrefix) {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            // GlobalFunction.WriteSystemLog ("GroupAPI  -   GetGroup - " + "Getting  group's data in Chat");
            try {
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);

                guId = (showPrefix + "_" + guId).ToLower ();

                string url = "https://api-" + regionKey + ".cometchat.io/v2.0/groups/" + guId;
                HttpResponseMessage response = client.GetAsync (url).Result;
                // GlobalFunction.WriteSystemLog ("GroupAPI  -   GetGroup - " + response.ToString ());
                //  GlobalFunction.WriteSystemLog ("GroupAPI  -   GetGroup - " + response.Content.ReadAsStringAsync ().Result);

                var responseContent = response.Content.ReadAsStringAsync ().Result;
                dynamic responseJson = JsonConvert.DeserializeObject (responseContent);
                if (response.IsSuccessStatusCode == true) {
                    return responseJson;
                }
                // GlobalFunction.WriteSystemLog ("GroupAPI  -   GetGroup - " + responseContent.ToString ());
            } catch (Exception ex) {
                Console.WriteLine (ex.Message.ToString ());
            }
            return result;
        }

        public static dynamic GetExistingGroup (string guId, string showPrefix, string userId) {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            try {
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);

                guId = (showPrefix + "_" + guId).ToLower ();
                string bhafUserId = (showPrefix + "_" + userId).ToLower ();

                string url = "https://api-" + regionKey + ".cometchat.io/v2.0/users/" + bhafUserId + "/groups?hasJoined=false&&searchKey=" + guId;
                HttpResponseMessage response = client.GetAsync (url).Result;

                var responseContent = response.Content.ReadAsStringAsync ().Result;
                dynamic responseJson = JsonConvert.DeserializeObject (responseContent);
                if (response.IsSuccessStatusCode == true) {
                    return responseJson;
                }
            } catch (Exception ex) {
                Console.WriteLine (ex.Message.ToString ());
            }
            return result;
        }

        public static dynamic RemoveMemberFromGroup (string userId, string guId, string showPrefix) {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            try {
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);

                guId = (showPrefix + "_" + guId).ToLower ();
                userId = (showPrefix + "_" + userId).ToLower ();

                string url = "https://api-" + regionKey + ".cometchat.io/v2.0/groups/" + guId + "/members/" + userId;
                HttpResponseMessage response = client.DeleteAsync (url).Result;

                var responseContent = response.Content.ReadAsStringAsync ().Result;
                dynamic responseJson = JsonConvert.DeserializeObject (responseContent);
                if (response.IsSuccessStatusCode == true) {
                    return true;
                }
            } catch (Exception ex) {
                Console.WriteLine (ex.Message.ToString ());
            }
            return result;
        }

    }
}