using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using iVE.DAL.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
namespace iVE.API.ChatAPI {
    public static class FriendAPI {
        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        //private static readonly HttpClient client = new HttpClient (httpClientHandler);
        public static dynamic AddFriend (string requestUserId, string responseUserId, string showPrefix) {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            // GlobalFunction.WriteSystemLog("FriendAPI  -   AddFriend - " + "Adding  friend in Chat");
            try {

                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);

                requestUserId = (showPrefix + "_" + requestUserId).ToLower ();
                responseUserId = (showPrefix + "_" + responseUserId).ToLower ();

                string[] acceptUser = new string[1];
                acceptUser[0] = requestUserId;
                AcceptList acceptObj = new AcceptList ();
                acceptObj.accepted = acceptUser;

                var content = new StringContent (JsonConvert.SerializeObject (acceptObj), Encoding.UTF8, "application/json");
                string url = "https://api-" + regionKey + ".cometchat.io/v2.0/users/" + responseUserId + "/friends";
                HttpResponseMessage response = client.PostAsync (url, content).Result;
                // GlobalFunction.WriteSystemLog("FriendAPI  -   AddFriend - " + response.ToString());
                //  GlobalFunction.WriteSystemLog("FriendAPI  -   AddFriend - " + response.Content.ReadAsStringAsync().Result);

                if (response.IsSuccessStatusCode == true) {
                    var responseContent = response.Content.ReadAsStringAsync ().Result;
                    dynamic responseJson = JsonConvert.DeserializeObject (responseContent);
                    if (responseJson.data.accepted != null) {
                        result = true;
                    }
                    //   GlobalFunction.WriteSystemLog("FriendAPI  -   AddFriend - " + responseContent.ToString());
                }
            } catch (Exception ex) {
                Console.WriteLine (ex.Message.ToString ());
            }
            return result;
        }

    }
}