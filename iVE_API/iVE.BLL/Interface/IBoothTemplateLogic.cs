using System.Threading.Tasks;
namespace iVE.BLL.Interface
{
    public interface IBoothTemplateLogic
    {
        dynamic GetAllBoothTemplate();
        dynamic GetBoothTemplateById(string exhId);
    }
}