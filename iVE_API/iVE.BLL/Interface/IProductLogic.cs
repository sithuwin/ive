using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
namespace iVE.BLL.Interface {
    public interface IProductLogic {
        string CreateOrUpdateProductByExhId (dynamic obj, string exhId, string productId, ref string id);
        void DeleteProductByProductId (string exhId, string productId);
        dynamic GetProductDetailsById (string exhId, string productId);
        dynamic GetProductInfoById (string productId);
        dynamic GetAllProductList (string exhId, dynamic obj);
        dynamic GetProductCommantListByProductId (string exhId, string productId);
        Task<string> UploadProductFile (IFormFile file, string productId);
        dynamic AddToMyFavourite (string userId, string productId);
        dynamic GetProductViewedCountByExhibitorId (string exhId);
        dynamic RemoveFavProduct (string userId, string productId);
        public string GetProductURL (string productId);
        dynamic GetAllProductList (string exhId);
        dynamic ViewProductByCountry (string exhId);

    }
}