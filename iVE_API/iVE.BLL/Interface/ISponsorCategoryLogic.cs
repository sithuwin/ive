namespace iVE.BLL.Interface {
    public interface ISponsorCategoryLogic {
        dynamic GetSponsorCategory ();
    }
}