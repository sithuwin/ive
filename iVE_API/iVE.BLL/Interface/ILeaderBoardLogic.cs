using System.IO;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using iVE.DAL.Models;
using System.Threading.Tasks;
namespace iVE.BLL.Interface
{
    public interface ILeaderBoardLogic
    {
       dynamic GetAllLeaderBoard(dynamic obj,string returnUrl);
       dynamic GetAllEntitlePoints(dynamic obj);
    }
}