namespace iVE.BLL.Interface {
    public interface IShowConfigLogic {
        dynamic GetShowConfig ();
        string CreateOrUpdateShowConfig (dynamic obj, string sponsorId);
        void DeleteShowConfigByName (string sponsorId);
    }
}