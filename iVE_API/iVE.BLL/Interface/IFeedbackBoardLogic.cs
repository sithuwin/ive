namespace iVE.BLL.Interface {
    public interface IFeedbackBoardLogic {
        dynamic GetFeedbacksbyThreadOwnerId (dynamic obj, string Id);
        dynamic GetAllComments (dynamic obj, string febId);
        void CreateComment (dynamic obj);
        void CreateCommentActions (dynamic obj);
        void UpdateCommentStatus (string febId, string cmtId, dynamic obj, string loginId);
    }
}