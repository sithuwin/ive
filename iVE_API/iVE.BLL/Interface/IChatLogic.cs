namespace iVE.BLL.Interface {
    public interface IChatLogic {
        bool JoinChatWithContactPerson (string exhId, string userId);
    }
}