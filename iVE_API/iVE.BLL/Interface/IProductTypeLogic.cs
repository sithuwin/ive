using Microsoft.AspNetCore.Http;
namespace iVE.BLL.Interface
{
    public interface IProductTypeLogic
    {
        dynamic GetProductTypeList();
    }
}