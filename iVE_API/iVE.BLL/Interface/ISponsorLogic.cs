namespace iVE.BLL.Interface
{
    public interface ISponsorLogic
    {
        dynamic GetSponsors();
        dynamic GetSponsor(string sponsorId);
        string CreateOrUpdateSponsor(dynamic obj, string sponsorId);
        void DeleteSponsorBySponsorId(string sponsorId);
        dynamic GetSponsorList(dynamic obj);
        dynamic GetSponsorsByCateGrouping(dynamic obj);
        dynamic GetSponsorDetails(string Name);
    }
}