using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
namespace iVE.BLL.Interface {
    public interface IExhibitorLogic {
        dynamic GetAllExhibitionList (dynamic obj);
        dynamic GetAllExhibitionListForOrganization (dynamic obj);
        dynamic GetExhibitorById (string Id);
        void UpdateExhibitorById (dynamic obj, string exhId);
        string CreateOrUpdateExhibior (dynamic obj, string exhId, ref string insertedNewExhID);
        dynamic GetVisitedVisitor (dynamic obj, string exhId, string status, string returnUrl, bool isExport);
        Task<string> UploadFile (IFormFile file, string exhId);

        void UpdateExhibitorStatus (string exhId, string status);

        #region ChatContactGroupMember (8-7-2020 th)
        void AddChatContactMember (string exhId, string userId);
        void RemoveChatContactMember (string exhId, string userId);
        dynamic GetChatContactMembers (string exhId);
        dynamic GetHallList ();
        dynamic GetExhibitorTypeList ();
        dynamic GetBoothList ();
        dynamic GetExhibitorCategoryList ();
        dynamic GetExhibitorList (dynamic obj);
        dynamic ExhibitorRecommendationReport (dynamic obj, bool isExport);
        dynamic VisitedAnalysisByCountry (string exhId);
        dynamic VisitedAnalysisByDay (string exhId);
        dynamic VisitedAnalysis (string exhId);
        dynamic ViewedExhProductByCountry (string exhId);
        dynamic FavouriteExhProduct (string exhId);
        dynamic GetExhibitorDashboardStatus (string exhId);
        dynamic GetVisitedVisitorList (dynamic obj, bool isExport);
        dynamic GetVisitedVisitorCountryList (dynamic obj, bool isExport);
        dynamic VisitorLoginAnalysisByCountry (dynamic obj, bool isExport);
        List<string> GetCountryList ();
        List<string> GetDayList ();
        dynamic GetExhibitorsByCountry ();
        dynamic GetExhibitorsStatus ();
        #endregion
        dynamic SaveExhibitorUpload (DataTable data, ref string msg);
        dynamic GetExhBoothTemplates ();
        string GetNotificationData (string userName);
        string CreateOrUpdateExhibiorOrg (dynamic obj, string exhId, ref string insertedNewExhID);
        dynamic GetDailyActiveUsers (dynamic obj);
        dynamic ExhEngagementReportForOrg (dynamic obj, bool isExport);
        dynamic ActivitySummaryReport (dynamic obj);
        bool ChangeExhibitorChatStatus (string userId, int type);

    }
}