using System.IO;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using iVE.DAL.Models;
using System.Threading.Tasks;
namespace iVE.BLL.Interface
{
    public interface IBoothLogic
    {
        dynamic GetAllBooth(string exhId);
        dynamic GetBoothById(string exhId, string boothId);
        string CreateOrUpdateBoothById(string exhId, string boothId, dynamic obj);
        dynamic GetBoothbyTemplateId(string templateId);
        void UpdateBoothFileConfig(dynamic obj, string exhId, string boothId);
        Task<string> UploadBoothFile(IFormFile file, string exhId, string boothId);

        /*8-7-2020*/
        void UpdateTemplateImageByExhID(dynamic obj, string exhId);
        Task<byte[]> GetBoothTemplateZipFile(string exhId);
    }
}