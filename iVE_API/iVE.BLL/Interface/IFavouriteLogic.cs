namespace iVE.BLL.Interface {
    public interface IFavouriteLogic {
        dynamic GetFavouriteExhibitors (string userid);
        void UpdateUsersFavourite(string actionId, string favouriteId,string favItemType);
    }
}