namespace iVE.BLL.Interface {
    public interface IContactPersonLogic {
        string AddOrUpdateContactPerson (dynamic obj, string exhId, string contactId, ref string insertedID);
        dynamic GetAllContactPersonByExhId (string exhId);
        void DeleteContactPersonByExhId (string exhId, string contactId);
        dynamic GetContactPersonById (string exhId, string contactId);
        dynamic GetContactPersonByType (string type);

        /*8-7-2020*/
        dynamic GetAllContactPersonsWithStatusByExhId (string exhId);
        void ChangeContactPersonPassword (dynamic obj);
        bool CheckBadgeEntitlement (string exhId);
        bool ChangeExhibitorGroupChatStatus (string exhId, int type);
    }
}