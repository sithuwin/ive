using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;

namespace CustomTokenAuthProvider
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseTokenProviderMiddleware(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<TokenProviderMiddleware>();
        }
    }
}
