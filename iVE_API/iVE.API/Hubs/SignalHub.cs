using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
namespace iVE.API
{
    public class SignalHub : Hub
    {
        private readonly static ConnectionMapping<string> _connections = new ConnectionMapping<string>();
        public Task ChangeUrl(string data)
        {
            return Clients.All.SendAsync("changeUrl", data);
        }
        public async Task SendMessage(string userId, string signalName, string signalMessage)
        {
            //Send Notification to specific User
            List<string> connectionIds = _connections.GetConnections(userId).ToList<string>();
            if (connectionIds.Count() > 0)
            {
                try
                {
                    await Clients.Clients(connectionIds).SendAsync(signalName, signalMessage);
                }
                catch (Exception) { }
            }
        }

        public override async Task OnConnectedAsync()
        {
            var httpContext = Context.GetHttpContext();
            if (httpContext != null)
            {
                try
                {
                    //Add Logged User
                    var keys = httpContext.Request.Query.Keys;
                    string userId = "";
                    foreach (var key in keys)
                    {
                        if (userId == "")
                        {
                            userId = key;
                            break;
                        }

                    }

                    //var UserAgent = httpContext.Request.Headers["User-Agent"].FirstOrDefault().ToString();
                    if (userId != null && userId != "")
                    {
                        var connId = Context.ConnectionId.ToString();
                        _connections.Add(userId, connId);

                        //Update Client
                        await Clients.All.SendAsync("UpdateUserList", null);
                    }
                }
                catch (Exception) { }
            }
        }

        public async Task AddUserIntoSignalConnection(string userId, string connId, IHubContext<SignalHub> _hubContext)
        {
            if (!String.IsNullOrEmpty(userId))
            {
                try
                {
                    //var connId = Context.ConnectionId.ToString();
                    _connections.Add(userId, connId);
                    //await Clients.Clients(connId).SendAsync("ConnectionSuccess");
                    await _hubContext.Clients.All.SendAsync("ConnectionSuccess", "Connected Successfully");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message.ToString());
                }
            }
        }
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var httpContext = Context.GetHttpContext();
            if (httpContext != null)
            {
                //Remove Logged User
                var username = httpContext.Request.Query["user"];
                var connId = Context.ConnectionId.ToString();
                _connections.Remove(username, Context.ConnectionId);
                await Clients.Clients(connId).SendAsync("ConnectionDisconnected", "Disconnected Successfully");
            }
        }
    }
}