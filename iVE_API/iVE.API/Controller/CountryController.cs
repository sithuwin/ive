using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Runtime.Intrinsics.X86;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System.Dynamic;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CountryController : BaseController
    {
        private ICountryLogic _countryLogic;
        public CountryController(ICountryLogic countryLogic)
        {
            _countryLogic = countryLogic;
        }

        [HttpGet("Countries")]
        public dynamic GetCountryList()
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _countryLogic.GetCountryList();
                return StatusCode(StatusCodes.Status200OK, data);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "CountryController - GetCountryList", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
    }
}