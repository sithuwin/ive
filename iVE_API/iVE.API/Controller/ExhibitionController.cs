using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ExhibitionController : BaseController
    {
        private ISponsorLogic _sponsorLogic;
        private IBoothTemplateLogic _boothTemplateLogic;
        private ISponsorCategoryLogic _sponsorCategoryLogic;
        private IShowConfigLogic _showConfigLogic;
        public ExhibitionController(ISponsorLogic sponsorLogic, IBoothTemplateLogic boothTemplateLogic, ISponsorCategoryLogic sponsorCategoryLogic
        , IShowConfigLogic showConfigLogic)
        {
            _sponsorLogic = sponsorLogic;
            _boothTemplateLogic = boothTemplateLogic;
            _sponsorCategoryLogic = sponsorCategoryLogic;
            _showConfigLogic = showConfigLogic;
        }

        #region Sponsor

        [HttpGet("Sponsor")]
        public dynamic GetSponsors()
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _sponsorLogic.GetSponsors();
                objResponse = new { data = data, success = true, message = "Successfully reterieved." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitionController - GetSponsors", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpGet("Sponsor/{SponsorID}")]
        public dynamic GetSponsor(string SponsorID)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _sponsorLogic.GetSponsor(SponsorID);
                objResponse = new { data = data, success = true, message = "Successfully reterieved." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitionController - GetSponsor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpDelete("Sponsors/{SponsorID}")]
        public dynamic DeleteSponsor(string SponsorID)
        {
            dynamic objResponse = null;
            string msg = "Successfully deleted.";
            try
            {
                _sponsorLogic.DeleteSponsorBySponsorId(SponsorID);
                objResponse = new { data = "", success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "ExhibitionController - DeleteSponsor", msg +", SponsorId:" + SponsorID, _tokenData.UserID);
                
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitionController - DeleteSponsor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        #endregion

        #region BoothTemplate
        [HttpGet("BoothTemplate")]
        public dynamic GetBoothTemplateList()
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _boothTemplateLogic.GetAllBoothTemplate();
                GlobalFunction.WriteSystemLog(LogType.Info, "ExhibitionController - GetBoothTemplateList", "Successfully reterieve.", _tokenData.UserID);
                objResponse = new { data = data, success = true, message = "Successfully reterieved." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitionController - GetBoothTemplateList", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("BoothTemplate/{Id}")]
        public dynamic GetBoothTemplateById(string Id)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _boothTemplateLogic.GetBoothTemplateById(Id);
                GlobalFunction.WriteSystemLog(LogType.Info, "ExhibitionController - GetBoothTemplateById", "Successfully reterieve. Id:" + Id , _tokenData.UserID);
                objResponse = new { data = data, success = true, message = "Successfully reterieved." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitionController - GetBoothTemplateById", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        #endregion

        #region SponsorCategory
        [Authorize(Roles = "Organizer")]
        [HttpGet("SponsorCategory")]
        public dynamic GetSponsorCategory()
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _sponsorCategoryLogic.GetSponsorCategory();
                objResponse = new { data = data, success = true, message = "Successfully reterieved." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitionController - GetSponsorCategory", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        #endregion

        #region ShowConfig
        [HttpGet("Info")]
        public dynamic GetShowConfig()
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _showConfigLogic.GetShowConfig();
                objResponse = new { data = data, success = true, message = "Successfully reterieved." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitionController - GetShowConfig", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }


        [HttpPost("Info/{Name}")]
        public dynamic CreateOrUpdateShowConfig([FromBody] JObject param, string Name)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            try
            {
                Name = _showConfigLogic.CreateOrUpdateShowConfig(obj, Name);
                objResponse = new { data = Name, success = true, message = "Successfully created." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitionController - CreateOrUpdateShowConfig", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to create." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpDelete("Info/{Name}")]
        public dynamic DeleteShowConfig(string Name)
        {
            dynamic objResponse = null;
            try
            {
                _showConfigLogic.DeleteShowConfigByName(Name);
                objResponse = new { data = "", success = true, message = "Successfully deleted." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitionController - DeletShowConfig", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        #endregion
    }
}