using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using iVE.API.ChatAPI;

namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ChatMessageReportController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public ChatMessageReportController(IRepositoryWrapper iw)
        {
            _repositoryWrapper = iw;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("Synchronize")]
        public dynamic SynchronizeChatMessage([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                string startTimeStamp = obj.startTimeStamp;
                dynamic isOk = SynchronizeMessagesFromCometchat(startTimeStamp);
                objResponse = new { data = isOk, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ChatMessageReportController - SynchronizeChatMessage", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost]
        public dynamic GetChatMessages([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "id";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";


                dynamic objResult = _repositoryWrapper.ChatMessageDetails.GetMessageList(obj, sortField, sortBy, false);

                objResponse = new { data = objResult, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ChatMessageReportController - GetChatMessages", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("Export")]
        public dynamic ExportChatMessages([FromBody] JObject param)
        {
            byte[] fileContents;
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "id";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";


                dynamic objResult = _repositoryWrapper.ChatMessageDetails.GetMessageList(obj, sortField, sortBy, true);
                List<string> Header = new List<string> { "Sender Name", "Receiver Name", "Message", "Receiver Type", "Send At" };
                List<string> fieldName = new List<string> { "senderName", "receiverName", "text", "receiverType", "sentDate" };

                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "ChatMessageList");
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"ChatMessageList_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ChatMessageReportController - ExportChatMessages", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private bool SynchronizeMessagesFromCometchat(string sendDate)
        {
            //User
            int limit = 300;
            bool userExist = true;
            bool groupExist = true;
            string nextUserMsgId = "";
            string nextGropMsgId = "";
            int i = 0;

            //User Synchronization From Comechat
            while (userExist)
            {
                string receiveType = "user";
                Console.Write(i++);
                dynamic userResult = MessageAPI.GetMessages(limit, receiveType, sendDate, nextUserMsgId);
                //dynamic userList = new JavaScriptSerializer().Deserialize<object>(userResult);//new ExpandedObjectFromApi(userResult);

                _repositoryWrapper.ChatMessageDetails.AddMessages(userResult.data);

                if (userResult.meta.previous != null)
                {
                    nextUserMsgId = userResult.meta.previous.id;
                }

                int currentCount = userResult.meta.current.count;
                int currentLimit = userResult.meta.current.limit;
                if (currentCount < currentLimit)
                {
                    userExist = false;
                }
            }

            //Group Synchronization From Comechat
            while (groupExist)
            {
                string receiveType = "group";
                dynamic userResult = MessageAPI.GetMessages(limit, receiveType, sendDate, nextGropMsgId);
                _repositoryWrapper.ChatMessageDetails.AddMessages(userResult.data);

                if (userResult.meta.previous != null)
                {
                    nextGropMsgId = userResult.meta.previous.id;
                }

                int currentCount = userResult.meta.current.count;
                int currentLimit = userResult.meta.current.limit;
                if (currentCount < currentLimit)
                {
                    groupExist = false;
                }
            }

            return true;
        }
    }
}