using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class FeedBackBoardController : BaseController
    {
        private IFeedbackBoardLogic _feedbackLogic;
        public FeedBackBoardController(IFeedbackBoardLogic feedbackLogic)
        {
            _feedbackLogic = feedbackLogic;
        }

        [HttpPost("{ThreadOwnerID}")]
        public dynamic GetFeedbackByThreadOwnerId([FromBody] JObject param, string ThreadOwnerID)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _feedbackLogic.GetFeedbacksbyThreadOwnerId(param, ThreadOwnerID);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "FeedBackBoardController - GetFeedbackByThreadOwnerId", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }

        }

        [HttpPost("{FebID}/Comments/All")]
        public dynamic GetAllComments([FromBody] JObject param, string FebID)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _feedbackLogic.GetAllComments(param, FebID);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "FeedBackBoardController - GetAllComments", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }
        }

        [HttpPost("{FebID}/Comments")]
        public dynamic CreateComment([FromBody] Newtonsoft.Json.Linq.JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                _feedbackLogic.CreateComment(obj);
                objResponse = new { data = "", success = true, message = "Successfully updated" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "FeedBackBoardController - CreateComment", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to updated" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("{FebID}/Comments/{CmtID}")]
        public dynamic UpdateCommentStatus([FromBody] Newtonsoft.Json.Linq.JObject param, string FebID, string CmtID)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                string userId = _tokenData.UserID;
                _feedbackLogic.UpdateCommentStatus(FebID, CmtID, obj, userId);
                objResponse = new { data = "", success = true, message = "Successfully updated" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "FeedBackBoardController - UpdateCommentsStatus", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to updated" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("{FebID}/Comments/CommentID/Actions")]
        public dynamic CreateCommentAction([FromBody] Newtonsoft.Json.Linq.JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                _feedbackLogic.CreateCommentActions(obj);
                objResponse = new { data = "", success = true, message = "Successfully updated" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "FeedBackBoardController - CreateCommentAction", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to updated" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

    }
}