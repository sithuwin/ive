using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class MCQScoreBaordController: BaseController
    {
        private IMCQScoreBoardLogic _mcqScoreBoardLogic;
        string returnUrl = "";
        public MCQScoreBaordController(IMCQScoreBoardLogic mcqScoreBoardLogic){
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            returnUrl = Configuration.GetSection("appSettings:ProfilePicUrl").Value;
            _mcqScoreBoardLogic = mcqScoreBoardLogic;
        }

        [HttpPost]
        public dynamic GetAllMCQScoreData([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _mcqScoreBoardLogic.GetAllMCQScoreData(obj,returnUrl);
                return StatusCode(StatusCodes.Status200OK, data);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "MCQScoreBaordController - GetAllMCQScoreData", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }
        }
    }
}