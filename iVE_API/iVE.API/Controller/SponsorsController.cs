using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using Microsoft.Extensions.Caching.Memory;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class SponsorsController : BaseController
    {
        private ISponsorLogic _sponsorLogic;
        private IMemoryCache _memoryCache;
        public SponsorsController(ISponsorLogic sponsorLogic,IMemoryCache memoryCache)
        {
            _sponsorLogic = sponsorLogic;
            _memoryCache = memoryCache;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{SponsorID}")]
        public dynamic CreateOrUpdateSponsor([FromBody] JObject param, string SponsorID)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            string message = "";
            try
            {
                message = _sponsorLogic.CreateOrUpdateSponsor(obj, SponsorID);
                if (message != "")
                {
                    objResponse = new { data = "false", success = false, message = message };
                }
                else
                {
                    ClearSponsorCache();
                    objResponse = new { data = "true", success = true, message = "Successfully updated" };
                }
                GlobalFunction.WriteSystemLog(LogType.Info, "SponsorsController - CreateOrUpdateSponsor", "SponsorId:" + SponsorID, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "SponsorsController - CreateOrUpdateSponsor", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to create" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost]
        public dynamic GetSponsors([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _sponsorLogic.GetSponsorList(obj);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "SponsorsController - GetSponsors", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }
        }

        [HttpPost("GroupTemplate")]
        public dynamic GetSponsorsByCateGrouping([FromBody] Newtonsoft.Json.Linq.JObject param)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            dynamic data = null;
            try
            {
                if (GlobalFunction.isRequireCreateCache ()){
                    dynamic sponsorList = new ExpandedObjectFromApi (ENUM_Cache.SponsorList);
                    string key = sponsorList.key;
                    if(!_memoryCache.TryGetValue(key,out data)){
                        data = _sponsorLogic.GetSponsorsByCateGrouping(obj);
                        GlobalFunction.CacheTryGetValueSet (key, data, _memoryCache);
                    }
                }else{
                    data = _sponsorLogic.GetSponsorsByCateGrouping(obj);
                }
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "SponsorsController - GetSponsorsByCateGrouping", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }
        }

        [HttpGet("{Name}")]
        public dynamic GetSponsorDetailsByName(string Name)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _sponsorLogic.GetSponsorDetails(Name);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "SponsorsController - GetSponsorDetailsByName", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        private void ClearSponsorCache()
        {
            dynamic data = null;
            dynamic keyObj = new ExpandedObjectFromApi(ENUM_Cache.SponsorList);
            string key = keyObj.key;
            if (_memoryCache.TryGetValue(key, out data))
            {
                GlobalFunction.CacheRemove(key, _memoryCache);
            }
        }
    }
}