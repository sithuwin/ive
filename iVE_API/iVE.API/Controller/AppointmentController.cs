using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class AppointmentController : BaseController
    {
        private IAppointmentLogic _appointmentLogic;
        private readonly IHubContext<SignalHub> _hubContext;
        private IRepositoryWrapper _repositoryWrapper;
        public AppointmentController(IAppointmentLogic appointmentLogic, IHubContext<SignalHub> hubContext, IRepositoryWrapper RW)
        {
            _appointmentLogic = appointmentLogic;
            _hubContext = hubContext;
            _repositoryWrapper = RW;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("CreateBaseTimeSlots")]
        public dynamic CreateTimeSlots([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string msg = "Successfully Created.";
            try
            {
                dynamic data = _appointmentLogic.CreateTimeSlots(obj);
                GlobalFunction.WriteSystemLog(LogType.Info, "AppointmentController - CreateTimeSlots", msg, _tokenData.UserID);
                objResponse = new { data = data, success = true, message = msg };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - CreateTimeSlots", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to created" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{OwnerID}/Schedule/{OtherID}")]
        public dynamic GetUserSchedule(string OwnerID, string OtherID)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _appointmentLogic.GetUserSchedule(OwnerID, OtherID);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - GetUserSchedule", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{OwnerID}/MySchedule")]
        public dynamic GetUserOwnSchedule(string OwnerID)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _appointmentLogic.GetUserOwnSchedule(OwnerID);
                objResponse = new { data = data, success = true, message = "Successfully reterieved." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - GetUserOwnSchedule", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{UserID}/ScheduleInfo/{TimeSlotId}")]
        public dynamic GetUserScheduleInfo(string UserID, string TimeSlotId)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _appointmentLogic.GetUserScheduleTimeSlot(UserID, TimeSlotId);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - GetUserScheduleInfo", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{OwnerID}/MySchedule/{TimeSlotId}/Request")]
        public dynamic GetMyScheduleRequest(string OwnerID, string TimeSlotId)
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _appointmentLogic.GetOwnerTimeSlotRequest(OwnerID, TimeSlotId);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - GetMyScheduleRequest", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("{OwnerID}/MySchedule/{TimeSlotId}/ReceivedRequestList")]
        public dynamic GetMyScheduleReceivedRequestList([FromBody] JObject param, string OwnerID, string TimeSlotId)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _appointmentLogic.GetOwnerTimeSlotReceivedRequestList(OwnerID, TimeSlotId, obj);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - GetMyScheduleReceivedRequestList", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("MakeRequest")]
        public async Task<dynamic> RequestForAppointment([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string msg = "Successfully Requested.";
            try
            {
                dynamic data = _appointmentLogic.RequestForAppointment(obj);
                if (data)
                {
                    string receiverId = obj.requestTo;
                    SendEmailToReceiver(receiverId);
                    await _hubContext.Clients.All.SendAsync("NotificationCount", "Check Your Notification Count");
                }
                objResponse = new { data = data, success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "AppointmentController - RequestForAppointment", msg, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - RequestForAppointment", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "Failed to Request!" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        private void SendEmailToReceiver(string receiverUserId)
        {

            tbl_Account receiverDetails = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == receiverUserId && x.deleteFlag == false).Select(x => x).FirstOrDefault();
            if (receiverDetails != null)
            {
                var emailConfig = _repositoryWrapper.EmailConfig.FindByCondition(x => x.em_Title == EmailTitle.AppointmentRequest).FirstOrDefault();
                if (emailConfig != null)
                {
                    dynamic emailSetting = new System.Dynamic.ExpandoObject();

                    string websiteLink = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.WebSiteLink && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    // string showName = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowName && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                    emailSetting.fromEmail = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPUserName && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    emailSetting.password = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPPassword && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    emailSetting.senderUserName = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowAdminName && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    emailSetting.port = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPPort && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    emailSetting.host = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPHost && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    emailSetting.enableSSL = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.EnableSSL && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    emailSetting.subject = emailConfig.em_Subject;
                    emailSetting.receiverUserName = receiverDetails.a_fullname;
                    string bmsg = GetEmailBody(emailConfig.em_Content, receiverDetails.a_fullname, websiteLink);
                    emailSetting.emessage = bmsg;
                    emailSetting.toEmail = receiverDetails.a_email;
                    GlobalFunction.SendEmail(emailSetting, true);
                }
            }
        }

        private string GetEmailBody(string message, string userName, string websitelink)
        {
            //message = message.Remove("\n",)
            StringBuilder emessage = new StringBuilder(message);
            emessage.Replace("<UserName>", userName);
            emessage.Replace("<websitelink>", websitelink);

            //emessage.Replace("\n",Environment.NewLine);
            return emessage.ToString();
        }

        [Authorize(Roles = "Exhibitor")]
        [HttpPost("RejectRequest")]
        public dynamic RejectRequestForAppointment([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string msg = "Successfully Canceled.";
            try
            {
                dynamic data = _appointmentLogic.RejectForRequst(obj);
                objResponse = new { data = data, success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "AppointmentController - RejectRequestForAppointment", msg, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - RejectRequestForAppointment", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "Failed to Cancel" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("CancelRequest")]
        public dynamic CancelRequestForAppointment([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string msg = "Successfully Canceled.";
            try
            {
                dynamic data = _appointmentLogic.CancelForRequst(obj);
                objResponse = new { data = data, success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "AppointmentController - CancelRequestForAppointment", msg, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - CancelRequestForAppointment", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "Failed to Cancel" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Exhibitor")]
        [HttpPost("AcceptRequest")]
        public async Task<dynamic> AcceptRequestForAppointment([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string msg = "Successfully Accepted.";
            string userName = "";
            string pName = "";
            string email = "";
            string exhId = "";
            string userType = "";
            try
            {
                string from = obj.requestFrom;
                string to = obj.requestTo;
                string timeSlotId = obj.timeSlotId;
                dynamic data = _appointmentLogic.AcceptForRequst(obj, _tokenData.UserID);
                if (data)
                {
                    await _hubContext.Clients.All.SendAsync("NotificationCount", "Check Your Notification Count");
                }
                if (data != null && data == true)
                {
                    var fromObj = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == from && x.deleteFlag == false).FirstOrDefault();
                    var toObj = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == to && x.deleteFlag == false).FirstOrDefault();
                    if (!string.IsNullOrEmpty(Convert.ToString(fromObj.ExhId)))
                    {
                        exhId = fromObj.ExhId;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(toObj.ExhId)))
                    {
                        exhId = toObj.ExhId;
                    }
                    var tsObj = _repositoryWrapper.AppTimeSlot.FindByCondition(x => x.TimeSlotID == timeSlotId && x.deleteFlag == false).FirstOrDefault();

                    if (fromObj != null)
                    {

                        userName = fromObj.a_fullname;
                        pName = toObj.a_fullname;
                        email = fromObj.a_email;
                        userType = fromObj.a_type;
                        SendEmail(userName, pName, email, tsObj, exhId, userType);
                    }
                    if (toObj != null)
                    {

                        userName = toObj.a_fullname;
                        pName = fromObj.a_fullname;
                        email = toObj.a_email;
                        userType = toObj.a_type;
                        SendEmail(userName, pName, email, tsObj, exhId, userType);
                    }
                }
                objResponse = new { data = data, success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "AppointmentController - AcceptRequestForAppointment", msg, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - AcceptRequestForAppointment", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "Failed to Accept!" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("CancelAppointment")]
        public dynamic CancelAppointment([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string msg = "Successfully Canceled.";
            try
            {
                dynamic data = _appointmentLogic.CancelForAppointment(obj);
                objResponse = new { data = data, success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "AppointmentController - CancelAppointment", msg, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - CancelAppointment", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "Failed to Cancel" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("MakeBlock")]
        public dynamic MakeBlock([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string msg = "Successfully Blocked.";
            try
            {
                dynamic data = _appointmentLogic.MakeBlockForTimeSlot(obj, _tokenData.UserID);
                objResponse = new { data = data, success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "AppointmentController - MakeBlock", msg, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - MakeBlock", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "Failed to block" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("CancelBlock")]
        public dynamic CancelBlock([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string msg = "Successfully Canceled.";
            try
            {
                dynamic data = _appointmentLogic.CanelBlockForTimeSlot(obj, _tokenData.UserID);
                objResponse = new { data = data, success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "AppointmentController - CancelBlock", msg, _tokenData.UserID);

                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - CancelBlock", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "Failed to Cancel" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        private bool SendEmail(string userName, string secondUserName, string userEmail, dynamic tsObj, string exhId, string userType)
        {
            bool result = false;
            string appDate = "";
            string appStartTime = "";
            string appEndTime = "";
            var obj = tsObj;
            try
            {

                dynamic emailSetting = new System.Dynamic.ExpandoObject();
                string showName = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowAdminName && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                emailSetting.fromEmail = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPUserName && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                emailSetting.password = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPPassword && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                emailSetting.senderUserName = showName;
                emailSetting.port = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPPort && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                emailSetting.host = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPHost && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                emailSetting.enableSSL = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.EnableSSL && x.Status == 1).Select(x => x.Value).FirstOrDefault();

                string EmailTemplate = EmailTitle.Delegate_App_Confirmed;
                if (userType == UserType.Exhibitor || userType == UserType.MainExhibitor)
                {
                    EmailTemplate = EmailTitle.Exhibitor_App_Confirmed;
                }

                var emailConfig = _repositoryWrapper.EmailConfig.FindByCondition(x => x.em_Title == EmailTemplate).FirstOrDefault();

                if (emailConfig != null)
                {
                    string bodyMsg = replaceBodyMessage(emailConfig.em_Content);
                    emailSetting.subject = showName + " " + emailConfig.em_Subject;
                    emailSetting.receiverUserName = userName;
                    if (obj != null)
                    {
                        string dateId = obj.Day;
                        var dateObj = _repositoryWrapper.Appointment.GetDay(dateId);
                        if (dateObj != null)
                        {
                            appDate = dateObj.d_date;
                        }
                        appStartTime = obj.StartTime;
                        appEndTime = obj.EndTime;
                    }

                    var exhObj = _repositoryWrapper.Exhibitor.FindByCondition(x => x.ExhID == exhId).FirstOrDefault();
                    string partnerName = "";
                    if (exhObj != null)
                    {
                        partnerName = exhObj.CompanyName;
                    }
                    string bmsg = GetEmailBody(bodyMsg, userName, partnerName, appDate, appStartTime, appEndTime, secondUserName);

                    emailSetting.toEmail = userEmail;
                    emailSetting.emessage = bmsg;
                    
                    GlobalFunction.SendEmail(emailSetting, true);
                    return true;
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AppointmentController - SendEmail", ex.Message.ToString(), _tokenData.UserID);
            }
            return result;
        }

        private string GetEmailBody(string message, string userName, string partnerName, string day, string stime, string etime, string secondUser)
        {
            //message = message.Remove("\n",)
            StringBuilder emessage = new StringBuilder(message);
            emessage.Replace("<UserName>", userName);
            emessage.Replace("<PartnerName>", partnerName);
            emessage.Replace("<StartTime>", (Convert.ToDateTime(stime)).ToString("HH:mm"));
            emessage.Replace("<EndTime>", (Convert.ToDateTime(etime)).ToString("HH:mm"));
            emessage.Replace("<Date>", (Convert.ToDateTime(stime)).ToString("dd/MM/yyyy"));
            emessage.Replace("<Booth Rep name>", secondUser);
            emessage.Replace("<Delegate>", secondUser);
            //emessage.Replace("\n",Environment.NewLine);
            return emessage.ToString();
        }

        private string replaceBodyMessage(string message)
        {
            message = message.Replace("\\n", "\n");

            return message;
        }
    }
}