using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class LeaderBoardController : BaseController
    {
        string returnUrl = "";
        private ILeaderBoardLogic _leaderBoardLogic;
        public LeaderBoardController(ILeaderBoardLogic leaderBoardLogic)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            returnUrl = Configuration.GetSection("appSettings:ProfilePicUrl").Value;
            _leaderBoardLogic = leaderBoardLogic;
        }

        [HttpPost]
        public dynamic GetAllLeaderBoard([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _leaderBoardLogic.GetAllLeaderBoard(obj, returnUrl);
                return StatusCode(StatusCodes.Status200OK, data);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "LeaderBoardController - GetAllLeaderBoard", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }
        }
        [HttpPost("EntitlePoints")]
        public dynamic GetAllEntitlePoints([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _leaderBoardLogic.GetAllEntitlePoints(obj);
                return StatusCode(StatusCodes.Status200OK, data);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "LeaderBoardController - GetAllEntitlePoints", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);

            }
        }
    }
}