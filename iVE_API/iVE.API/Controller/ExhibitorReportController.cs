using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using iVE.API;
using iVE.BLL.Interface;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace iVE.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ExhibitorReportController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        private IExhibitorLogic _exhibitorLogic;
        private IProductLogic _productLogic;
        public ExhibitorReportController(IRepositoryWrapper iw, IExhibitorLogic exhibitorlogic, IProductLogic productLogic)
        {
            _repositoryWrapper = iw;
            _exhibitorLogic = exhibitorlogic;
            _productLogic = productLogic;
        }

        #region Exhibitor Report
        [Authorize(Roles = "Organizer")]
        [HttpPost("Reports/Recommendation")]
        public dynamic ExhibitorRecommendationReport([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            try
            {
                dynamic data = _exhibitorLogic.ExhibitorRecommendationReport(obj, false);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - ExhibitorRecommendationReport", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("Reports/CountryVisitedVisitor")]
        public dynamic VisitedAnalysisCountryList([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _exhibitorLogic.GetVisitedVisitorCountryList(obj, false);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - VisitedAnalysisCountryList", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("Reports/VisitedVisitors")]
        public dynamic GetVisitedVisitorList([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _exhibitorLogic.GetVisitedVisitorList(obj, false);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - GetVisitedVisitorList", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("Reports/CountryAnalysis")]
        public dynamic VisitorLoginAnalysisByCountry([FromBody] JObject param)
        {
            //UserLogin analysis by day for each country
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _exhibitorLogic.VisitorLoginAnalysisByCountry(obj, false);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - VisitorLoginAnalysisByCountry", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("Reports/ExhibitorCountryGroup")]
        public dynamic GetExhibitorsByCountry()
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _exhibitorLogic.GetExhibitorsByCountry();
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - GetExhibitorsByCountry", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("Reports/ExhibitorStatusGroup")]
        public dynamic GetExhibitorStatus()
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _exhibitorLogic.GetExhibitorsStatus();
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - GetExhibitorStatus", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        #endregion

        #region  Exhibitor Product Report
        [HttpGet("Reports/ViewProductCountry")]
        public dynamic ViewProductByCountry()
        {
            dynamic objResponse = null;
            try
            {
                //Get view product by country for current exhibitor
                string exhId = _repositoryWrapper.Account.FindByCondition(x => x.deleteFlag == false && x.a_ID == _tokenData.UserID && x.a_type == UserType.MainExhibitor).Select(x => x.ExhId).FirstOrDefault();
                dynamic data = _productLogic.ViewProductByCountry(exhId);
                objResponse = new { data = data, success = true, message = "Successfully reterieve." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - ViewProductByCountry", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        #endregion

        #region  ExcelExport
        [Authorize(Roles = "Organizer")]
        [HttpPost("ExcelExport/VisitedVisitors")]
        public dynamic ExportVisitedVisitorList([FromBody] JObject param)
        {
            byte[] fileContents;
            dynamic obj = param;
            try
            {
                var objResult = _exhibitorLogic.GetVisitedVisitorList(obj, true);
                List<string> Header = new List<string> {  "Visitor Name","Country", "Email", "Designation" ,"Telephone", "Exhibitor Name", "Visitor Company Name" };
                List<string> fieldName = new List<string> { "FullName", "Country", "Email", "Designation", "Telephone","ExhibitingCompany", "visitorCompany" };

                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "VisitedVisitorList");
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }

                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"VisitedVisitorList_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - ExportVisitedVisitorList", ex.Message.ToString(), _tokenData.UserID);
                // objResponse = new { data = "", success = false, message = "Failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            

        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("ExcelExport/Recommendation")]
        public dynamic ExportExhibitorRecommendation([FromBody] JObject param)
        {
            byte[] fileContents;
            dynamic obj = param;
            try
            {
                var objResult = _exhibitorLogic.ExhibitorRecommendationReport(obj, true);
                List<string> Header = new List<string> { "ExhibitingCompany", "Recommended", "MeetUp", "Not Turnup" };
                List<string> fieldName = new List<string> { "ExhibitingCompany", "Recommended", "MeetUp", "NotTurnup" };

                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "ExhRecommendationList");
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }

                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"ExhRecommendationList_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - ExportExhibitorRecommendation", ex.Message.ToString(), _tokenData.UserID);
                // objResponse = new { data = "", success = false, message = "Failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("ExcelExport/CountryVisitedVisitors")]
        public dynamic ExportVisitedVisitorCountry([FromBody] JObject param)
        {
            byte[] fileContents;
            dynamic obj = param;
            try
            {
                var objResult = _exhibitorLogic.GetVisitedVisitorCountryList(obj, true);

                //Column Header
                List<string> Header = new List<string>();
                Header.Add("ExhibitingCompany");
                List<string> headerList = _exhibitorLogic.GetCountryList();
                Header.AddRange(headerList);

                //Column Fields
                List<string> fieldName = new List<string>();
                fieldName.Add("ExhibitingCompany");
                List<string> fieldNameList = _exhibitorLogic.GetCountryList();
                fieldName.AddRange(fieldNameList);

                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "VisitedVisitorAnalysisByCountry");
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }

                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"VisitedVisitorAnalysisByCountry_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - ExportVisitedVisitorCountry", ex.Message.ToString(), _tokenData.UserID);
                // objResponse = new { data = "", success = false, message = "Failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("ExcelExport/CountryAnalysis")]
        public dynamic ExportVisitorLoginAnalysisByCountry([FromBody] JObject param)
        {
            byte[] fileContents;
            dynamic obj = param;
            try
            {
                var objResult = _exhibitorLogic.VisitorLoginAnalysisByCountry(obj, true);

                //Column Header
                List<string> Header = new List<string>();
                Header.Add("Country");
                List<string> headerList = _exhibitorLogic.GetDayList();
                Header.AddRange(headerList);

                //Column Fields
                List<string> fieldName = new List<string>();
                fieldName.Add("Country");
                List<string> fieldNameList = _exhibitorLogic.GetDayList();
                fieldName.AddRange(fieldNameList);

                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "VisitorLoginAnalysisByCountry");
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }

                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"VisitorLoginAnalysisByCountry_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - ExportVisitorLoginAnalysisByCountry", ex.Message.ToString(), _tokenData.UserID);
                // objResponse = new { data = "", success = false, message = "Failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion

        [Authorize(Roles = "Organizer")]
        [HttpPost("Reports/DailyActiveUsers")]
        public dynamic GetDailyActiveUsers([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                dynamic data = _exhibitorLogic.GetDailyActiveUsers(obj);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - GetDailyActiveUsers", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

    
        [Authorize(Roles = "Organizer")]
        [HttpPost("ExhEngagementDetail")]
        public dynamic ExhEngagementDetailForOrganizer([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            try
            {
                dynamic data = _exhibitorLogic.ExhEngagementReportForOrg(param,false);
                //objResponse = new { data = data, success = true, message = "Successfully reterieve." };
                return StatusCode(StatusCodes.Status200OK, data);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - ExhEngagementDetailForOrganizer", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("ExcelExport/ExhEngagementDetail")]
        public dynamic ExhEngagementDetailExportForOrganizer([FromBody] JObject param)
        {
            byte[] fileContents;
            dynamic obj = param;
            try
            {
                var objResult = _exhibitorLogic.ExhEngagementReportForOrg(obj, true);
                List<string> Header = new List<string> { "Exhibiting Company", "Reach", "Like", "Appointment","Engagement" };
                List<string> fieldName = new List<string> { "company", "Reach", "Like", "Appointment","Engagement" };

                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "ExhEngagementDetailList");
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"ExhEngagementDetailList_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - ExhEngagementDetailExportForOrganizer", ex.Message.ToString(), _tokenData.UserID);
                // objResponse = new { data = "", success = false, message = "Failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("ActiveSummary")]
        public dynamic ActiveSummaryReport([FromBody] Newtonsoft.Json.Linq.JObject param)
        {
            dynamic objResponse = null;
            dynamic objparam = param;
            try
            {
                dynamic data = _exhibitorLogic.ActivitySummaryReport(objparam);
                objResponse = new { data = data, success = true, message = "Successfully retrieved." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ExhibitorReportController - ActiveSummaryReport", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

    }
}