namespace iVE.DAL.Models
{
    public class NotificationType
    {
        public static string AppointmentRequest = "Appointment Request";
        public static string AppointmentConfirm = "Appointment Confirm";
        public static string NotifyVisitedVisitor = "Chat Notifications from Exhibitor";
    }

    public class NotificationStauts
    {
        public static string Send = "Send";
        public static string Error = "ERROR";
    }
}