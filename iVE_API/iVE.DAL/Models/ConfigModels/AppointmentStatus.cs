namespace iVE.DAL.Models
{
    public class AppointmentStatus
    {
        public static string Block = "B";
        public static string Available = "AVL";
        public static string Confirmed = "COM";
        public static string Requested = "R";
        public static string ReceivedRequest = "RR";
    }

    public class AppointmentCondition
    {
        public static string BlockUser = "B";
        public static string AppointmentConfirmed = "APP";
    }

    public class AppointmentBookingType
    {
        public static string VisterRequest = "VR";
        public static string ExhibitorRequst = "SR";
        public static string OrganizerBlock = "OB";
        public static string ExhibitorBlock = "EB";
        public static string VisitorBlock = "VB";
        public static string PreScheduled = "PS";
    }

    public class RequestCondition
    {
        public static string Completed = "COM";
        public static string InProcess = "IP";
        public static string Cancel = "CANCEL";
        public static string Reject = "REJ";
    }

    public class RequestAction
    {
        public static string Accept = "ACCEPT";
    }

    public class RequestBookingType
    {
        public static string VisterRequest = "VR";
        public static string ExhibitorRequst = "SR";
    }

    public class AppointmentUserType
    {
        public static string Organizer = "O";
        public static string Exhibitor = "E";
        public static string Visitor = "V";
    }

    public class TimeSlotGroupType
    {
        public static string Default = "Default";
    }
}