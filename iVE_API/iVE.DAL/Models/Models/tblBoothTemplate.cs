using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models {
    [Table ("tblBoothTemplate")]
    public class tblBoothTemplate {
        public string BoothID { get; set; }
        public string BoothName { get; set; }
        public string BoothImage { get; set; }
        public string BoothConfig { get; set; }
    }
}