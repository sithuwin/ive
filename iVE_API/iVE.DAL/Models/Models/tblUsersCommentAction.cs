using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblUsersCommentAction")]
    public class tblUsersCommentAction
    {
        public int Id { get; set; }
        public int CommentID { get; set; }
        public string UserId { get; set; }
        public string UserActions { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public  DateTime? ModifiedDate { get; set; }
        public bool deleteFlag { get; set; }
    }
}