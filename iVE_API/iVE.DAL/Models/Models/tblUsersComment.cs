using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblUsersComment")]
    public class tblUsersComment
    {
        public int CommentID { get; set; }
        public  string FebID { get; set; }
        public string UserID { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public  DateTime? ModifiedDate { get; set; }
        public string UpdatedBy { get; set; }
        public bool deleteFlag { get; set; }
    }
}