using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblShowSponsors")]
    public class tblShowSponsors
    {
        public int ID { get; set; }
        public string sponsorID { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime createdDate { get; set; }
        public string d_ID { get; set; }
    }
}