using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblExhibitorProduct")]
    public class tblProduct
    {
        public string p_ID { get; set; }
        public int? sororder { get; set; }
        public string ExhID { get; set; }
        public string p_name { get; set; }
        public string Description { get; set; }
        public string p_category { get; set; }
        public string p_mainproductID { get; set; }
        public string URL { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public string UpdatedDate { get; set; }
        public string p_video { get; set; }

    }
}