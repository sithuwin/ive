using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblFeedbackboard")]
    public class tblFeedbackboard
    {
        public string FebId { get; set; }
        public string ThreadOwnerID { get; set; }
        public string PostType { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool deleteFlag { get; set; }
    }
}