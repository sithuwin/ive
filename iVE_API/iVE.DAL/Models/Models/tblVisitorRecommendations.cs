using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblVisitorRecommendations")]
    public class tblVisitorRecommendations
    {
        public string VisitorID{ get; set; }
        public string ExhID { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}