using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tbl_App_MySchedule")]
    public class tbl_App_MySchedule
    {
        public string OwnerID { get; set; }
        public string AppointmentID { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool deleteFlag { get; set; }
    }
}