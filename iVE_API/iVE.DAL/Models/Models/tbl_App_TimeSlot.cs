using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tbl_App_TimeSlot")]
    public class tbl_App_TimeSlot
    {
        public string TimeSlotID { get; set; }
        public string Day { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string TimeSlotGroupType { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
        public bool deleteFlag { get; set; }
    }
}