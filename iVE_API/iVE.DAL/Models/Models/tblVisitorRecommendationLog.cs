using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table ("tblVisitorRecommendationLog")]
    public class tblVisitorRecommendationLog
    {
        public string VisitorID{ get; set; }
        public string ExhID { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}