using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models {
    [Table ("tblExhibitingCompany")]
    public class tblExhibitingCompany {
        public string ec_ID { get; set; }
        public string ec_name { get; set; }
        public string ec_address { get; set; }
        public string ec_country { get; set; }
        public string ec_email { get; set; }
        public string ec_contactno { get; set; }
        public string ec_website { get; set; }
        public string ec_brochure { get; set; }
        public string ec_logo { get; set; }
        public string ec_companyprofile { get; set; }
        public string ec_companyType { get; set; }
        public string ec_hall { get; set; }
        public string ec_booth { get; set; }
        public string contactpersonID { get; set; }
        public string ec_product { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public string UpdatedDate { get; set; }
        public string ec_marketseg { get; set; }
        public string ec_trends { get; set; }
        public string clientID { get; set; }
        public string ec_productHTML { get; set; }
        public string isPromotion { get; set; }
    }
}