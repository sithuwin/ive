using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblExhibitorProductType")]
    public class tblExhibitorProductType
    {
        public int ID { get; set; }
        public string ExhID { get; set; }
        public string ProductID { get; set; }
        public string ProductCategory { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}