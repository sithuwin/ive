using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblExhibitorCategory")]
    public class tblExhibitorCategory
    {
        public int ID { get; set; }
        public string Category { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}