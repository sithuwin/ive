using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iVE.DAL.Models
{
    [Table("tblExhibitorType")]
    public class tblExhibitorType
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public int OrderNo { get; set; }
        public int? entitleBadges { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}