using System.Collections.Generic;
namespace iVE.DAL.Models
{
    public class RegistrantStatus
    {

        public string action { get; set; }
        public List<Registrants> registrants { get; set; }
    }

    public class Registrants
    {
        public string id { get; set; }
        public string email { get; set; }
    }
}