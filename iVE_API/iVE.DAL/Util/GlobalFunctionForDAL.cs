using Microsoft.Extensions.Configuration;
using System.Text;
using System.Threading.Tasks;
namespace iVE.DAL.Util {
    public static class GlobalFunctionForDAL {
        public static string GenerateSalthKey (string password) {
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string salt = Configuration.GetSection ("SaltKey").Value;
            return DataAccess.SaltedHash.ComputeHash (salt, password);
        }

    }
}