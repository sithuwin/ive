namespace iVE.DAL.Util {
    public static class Constants {

    }

    public static class SiteRunNumbers {
        public const string AdminRunKey = "ADM_";
        public const string UserRunKey = "U_";

        public const string AccountRunKey = "A";
        public const string ContactPersonRunKey = "CTP";
        public const string ProductRunKey = "P";
        public const string BoothTemplateRunKey = "BT";
        public const string ExhibitorRunKey = "Exh";
        public const string SponsorRunKey = "SPN";
        public const string AppointmentRunKey = "APP";
        public const string TimeSlotRunKey = "TS";
    }
    public static class UserType {
        public const string Delegate = "D";
        public const string Speaker = "S";
        public const string Visitor = "V";
        public const string Organizer = "O";
        public const string MainExhibitor = "ME";
        public const string Exhibitor = "E";
        public const string VIP = "VIP";
        public const string Media = "M";
        public const string FreeDelegate = "FD";
    }
    public static class ShowConfigNames {
        public const string ShowName = "ShowName";
        public const string ShowPrefix = "ShowPrefix";
        public const string ConferenceName = "ShowName";
        public const string Category = "Category";
        public const string Country = "Country";
        public const string Link = "Link";
        public const string SMTPUserName = "SMTPUserName";
        public const string SMTPPassword = "SMTPPassword";
        public const string SMTPPort = "SMTPPort";
        public const string SMTPHost = "SMTPHost";
        public const string EnableSSL = "EnableSSL";
        public const string ShowAdminName = "ShowAdminName";
        public const string WebSiteLink = "WebSiteLink";
        public const string VisitorChatRequestStatus = "VisitorChatRequestStatus";
    }

    public static class AuditLogType {
        public const string login = "LIN";
        public const string logout = "LOUT";
        public const string ViewMaterialDescription = "V_MATRLDesc";
        public const string ViewMaterial = "V_MATR";
        public const string JoinConference = "J_CONF";
        public const string JoinGroupChat = "J_GCHAT";
        public const string DownloadVCard = "D_VCARD";
        public const string ViewPoster = "V_POSTR";
        public const string ViewPosterDescription = "V_PDESC";
        public const string VisitExhibitor = "V_EXH";
        public const string ExitExhibitor = "Exit_EXH";
        public const string ViewProduct = "V_PRO";
        public const string ChangedContactPersonPwd = "C_ContactPerPwd";
        public const string Started_JoinNetworkLogGroupChat = "CHAT_NetLogGROUP_STARTED";
        public const string Ended_JoinNetworkLogGroupChat = "CHAT_NetLogGROUP_ENDEDED";
        public const string Started_ChatWithExhibitor = "CHAT_EXH_STARTED";
        public const string Ended_ChatWithExhibitor = "CHAT_EXH_ENDED";
        public const string Started_JoinExhibitorGroupChat = "CHAT_EXHGROUP_STARTED";
        public const string Ended_JoinExhibitorGroupChat = "CHAT_EXHGROUP_ENDED";
        public const string Started_JoinHelpDeskChat = "CHAT_HELPDESK_STARTED";
        public const string Ended_JoinHelpDeskChat = "CHAT_HELPDESK_ENDED";
        public const string Started_JoinCONNECTIONChat = "CHAT_CONN_STARTED";
        public const string Ended_JoinCONNECTIONChat = "CHAT_CONN_ENDED";

    }
    public static class DeviceType {
        public const string Mobile = "Mobile";
        public const string Web = "Web";
        public const string Window = "Window";
        public const string Android = "Android";
        public const string IOS = "IOS";
    }
    public static class ContactPersonType {
        public static string MainExhibitor = "ME";
        public static string ContactPerson = "E";
    }
    public static class Status {
        public static string Pending = "Pending";
        public static string Cancel = "Cancel";
        public static string Block = "Block";
        public static string Reject = "Reject";
        public static string Done = "Done";
        public const string Send = "Send";
        public const string ERROR = "ERROR";
        // public const string Request = "Request";
        public const string Queu = "Q";
        public const string Complete = "Com";
        public const string Approve = "Approve";
        public const string Close = "Close";
        public const string Open = "Open";
        public const string Delete = "Delete";
    }

    public static class FavouriteItem {
        public static string Product = "P";
        public static string Exhibitor = "E";
        public static string Visitor = "V";
        public static string Conference = "C";
    }

    public static class ExhibitorStatus {
        public static string Pending = "Pending";
        public static string Submitted = "Submitted";
        public static string Approved = "Approved";
        public static string Published = "Published";
        public static string InitialState = "Initial State";
    }
    public static class UserCommentStatus {
        public static string Pending = "Pending";
        public static string Approved = "Approved";
        public static string Cancelled = "Cancelled";
    }

    public static class PostType {
        public static string Conference = "C";
        public static string Room = "R";
        public static string Booth = "B";
    }
    public static class UserActions {
        public static string Like = "L";
        public static string Unlike = "UL";
        public static string ReplyComment = "RepC";
    }
    public static class UserBlockRule {
        public static string Exh_Block = "ExhibitorBlock";
    }

    public static class EmailTitle {
        public static string Delegate_App_Confirmed = "Delegate Appointment Confirmed";
        public static string Exhibitor_App_Confirmed = "Exhibitor Appointment Confirmed";
        public static string ChatRequest = "VisitorChatRequest";
        public static string AppointmentRequest = "AppointmentRequest";
    }

    public static class ConferenceStructureType {
        public const string MainConference = "M";
        public const string Workshop = "W";
        public const string Room = "ROOM";
    }

}