using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface ILeaderBoardRepository : IRepositoryBase<tblLeaderBoard>
    {
        dynamic GetAllLeaderBoard(dynamic obj,string returnUrl);
        dynamic GetAllEntitlePoints(dynamic obj);
    }
}