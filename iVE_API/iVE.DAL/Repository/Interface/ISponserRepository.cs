using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface {
    public interface ISponserRepository : IRepositoryBase<tblSponsor> {
        dynamic GetSponsors ();
        void SaveNewSponsor(dynamic obj, tblSponsor sponsor);
        void UpdateSponsor (dynamic obj, tblSponsor sponsor);
        void DeleteSponsor (tblSponsor sponsor);
        dynamic GetSponsorList(dynamic obj);
        dynamic GetSponsorListByCateGrouping(dynamic obj);
        dynamic GetSponsorDetails(string Name);
        dynamic GetSponsorById(string Id);
    }
}