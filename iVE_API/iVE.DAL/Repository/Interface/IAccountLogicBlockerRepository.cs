using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IAccountLogicBlockerRepository : IRepositoryBase<tblAccountLoginBlocker>
    {
    }
}