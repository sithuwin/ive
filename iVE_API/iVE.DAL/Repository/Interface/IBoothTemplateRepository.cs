using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface {
    public interface IBoothTemplateRepository : IRepositoryBase<tblBoothTemplate> {
        dynamic GetAllBoothTemplate ();
        dynamic GetBoothTemplateById (string Id);
        dynamic GetTemplateByExhId(string Id);
    }
}