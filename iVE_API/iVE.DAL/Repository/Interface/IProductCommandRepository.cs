using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface {
    public interface IProductCommantRepository : IRepositoryBase<tblProductCommant> {
        dynamic GetProductCommantList (string exhId, string productId);
    }
}