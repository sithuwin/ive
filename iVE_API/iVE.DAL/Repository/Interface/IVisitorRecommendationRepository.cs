using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IVisitorRecommendationRepository:IRepositoryBase<tblVisitorRecommendations>
    {
        dynamic GetAllRecommendedExhibitorsByVisitor(string vId);
        void AddVisitorRecommendation(string vId, string exhID);
        void AddVisitorRecommendationLog(tblVisitorRecommendations vistorRec);
    }
}