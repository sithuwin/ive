using System.Collections.Generic;
using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IExhibitorRepository : IRepositoryBase<tblExhibitor>
    {
        dynamic GetAllExhibitionList(dynamic obj, string sortField, string sortBy);
        dynamic GetAllExhibitionListForOrg(dynamic obj, string sortField, string sortBy);
        void UpdateExhibitor(dynamic obj, tblExhibitor exhibitor);

        dynamic GetExhibitorData(string exhId);
        void CreateExhibitor(dynamic obj, string exhId);
        dynamic GetVisitedVisitor(dynamic obj, string sortField, string sortBy, string exhId, string status, string returnUrl, bool isExport);
        void UpdateExhibitorStatusByExhibitorId(string exhId, string status, tblExhibitor exhibitor);
        void AddChatContactMember(string exhId, string userId, tblExhibitor exhibitor);
        void RemoveChatContactMember(string exhId, string userId, tblExhibitor exhibitor);
        dynamic GetChatContactMembers(string exhId);
        dynamic GetHallList();
        dynamic GetExhibitorTypeList();
        dynamic GetBoothList();
        dynamic GetExhibitorCategory();
        dynamic GetExhibitors(dynamic obj, string sortField, string sortBy);
        dynamic ExhibitorRecommendationReport(dynamic obj, bool isExport);
        dynamic VisitedAnalysisByCountry(string exhId);
        dynamic VisitedAnalysisByDay(string exhId);
        dynamic VisitedAnalysis(string exhId);
        dynamic ViewedExhProductByCountry(string exhId);
        dynamic FavouriteExhProduct(string exhId);
        dynamic GetExhibitorDashboardStatus(string exhId);
        dynamic GetVisitedVisitorList(dynamic obj, bool isExport);
        dynamic GetVisitedVisitorCountryList(dynamic obj, bool isExport);
        dynamic VisitorLoginAnalysisByCountry(dynamic obj, bool isExport);
        List<string> GetCountryList();
        List<string> GetDayList();
        dynamic GetExhibitorsByCountry();
        dynamic GetExhibitorsStatus();
        string GetExhibitorTypeIDByType(string countryName);
        dynamic GetExhBoothTemplates();
        string GetExistingGroupMemberId(string exhId);
        void UpdateExhibitorOrg(dynamic obj, tblExhibitor exhibitor);
        dynamic GetDailyActiveUsers(dynamic obj);
        dynamic ExhEngagementDetails(dynamic obj,bool isexport);
        dynamic ActivitySummaryReport(dynamic obj);
    }
}