using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface {
    public interface IShowConfigRepository : IRepositoryBase<tblShowConfig> {
        dynamic GetShowConfig ();
        void SaveNewShowConfig (dynamic obj, string name);
        void UpdateShowConfig (dynamic obj, tblShowConfig showconfig);
        void DeleteShowConfig (tblShowConfig showconfig);
    }
}