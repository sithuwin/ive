using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IExhibitingCompanyRepository:IRepositoryBase<tblExhibitingCompany>
    {
         
    }
}