using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IProductRepository : IRepositoryBase<tblProduct>
    {
        void SaveNewProduct(dynamic obj, string exhId, string productId);
        void UpdateProduct(dynamic obj, tblProduct product);
        void DeleteProduct(tblProduct product);
        void updateProductFileUrl(tblProduct product, string url);
        dynamic addToFavProduct(string userId, string productId);
        dynamic GetProductViewedCountByExhibitorId (string exhId);
        dynamic UpdateFavProductByVisitor(tbl_UsersFavourite favProductObj,int changedStatus,string userId);
        dynamic GetProductsByExhId(string exhId,dynamic obj);
        dynamic ViewProductByCountry(string exhId);
        dynamic GetProductDetails(string exhID,string pID);
    }
}