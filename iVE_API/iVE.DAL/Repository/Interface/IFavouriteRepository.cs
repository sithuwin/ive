using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface {
    public interface IFavouriteRepository : IRepositoryBase<tbl_UsersFavourite> {
        dynamic GetFavouriteExhibitors (string userid);

        void SaveNewUserFavourite(string actionId, string favouriteId,string favType);
        void UpdateUserFavourite(tbl_UsersFavourite obj,int _status);
    }
}