using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface {
    public interface ISponsorCategoryRepository : IRepositoryBase<tblSponsorCategory> {
        dynamic GetSponsorCategory ();
    }
}