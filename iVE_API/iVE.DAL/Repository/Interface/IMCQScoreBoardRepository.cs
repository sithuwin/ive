using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IMCQScoreBoardRepository: IRepositoryBase<tblMCQScoreBoard>
    {
        dynamic GetAllMCQScoreData(dynamic obj,string returnUrl);
    }
}