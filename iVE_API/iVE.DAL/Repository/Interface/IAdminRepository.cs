using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface {
    public interface IAdminRepository : IRepositoryBase<tbl_Administrator> {
    }
}