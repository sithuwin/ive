using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface ISiteRunNumberRepository : IRepositoryBase<SiteRunNumber>
    {
        string GetNewId(string runKey);
        void UpdateNumberByRunKey(string runKey);
        SiteRunNumber GetRunNumberPrefixAndCurrentNo(string runKey);
        void UpdateNumberByRunKeyAndCurrentNo(string runKey, int currentNo);
    }
}