using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface IAppointmentRepository : IRepositoryBase<tbl_Appointments>
    {
        dynamic GetActiveEventDate();
        dynamic CreateTimeSlots(dynamic days, dynamic starttimes, int duration, string prefix, int currentNo, int runno_incr);
        dynamic GetUserSchedule(string ownerId, string userId, string curretnUserType);
        dynamic GetUserOwnSchedule(string ownerId);
        dynamic GetUserScheduleInfo(string userId, string timeSlotId);
        dynamic GetOwnerRequest(string ownerId, string timeSlotId);
        dynamic GetOwnerReceivedRequestList(string ownerId, string timeSlotId, dynamic obj, string sortField, string sortBy);
        dynamic CreateRequest(dynamic obj, string userType);
        dynamic UpdateRequest(dynamic obj, tbl_App_Request requestObj);
        dynamic GetAppointmentUserType(string userId);
        dynamic GetActiveRequest(string requestFrom, string requestTo, string timeSlotId);
        dynamic GetActiveRequest(string requestFrom, string timeSlotId);
        dynamic RejectAllReceivedRequest(string requestFrom, string requestTo, string timeSlotId);
        dynamic CreateAppointment(string appointmentId, string timeSlotId, string appType, string bookingType, string tokenUser, string description);
        dynamic UpdateAppointment(tbl_Appointments appointmentObj);
        dynamic CreateMySchedule(string appointmentId, string ownerId);
        dynamic UpdateAllMyScheduleByAppointmentId(string appointmentId);
        dynamic IsFreeAtRequestedTime(string timeSlotId, string userId);
        dynamic GetActiveAppointment(string timeSlotId, string userId);
        dynamic GetTimeSlotDetail(string timeSlotId);
        dynamic GetDay(string dateId);
    }
}