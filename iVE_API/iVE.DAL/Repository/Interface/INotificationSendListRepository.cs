using iVE.DAL.Models;
namespace iVE.DAL.Repository.Interface
{
    public interface INotificationSendListRepository : IRepositoryBase<tblNotificationSendList>
    {
        dynamic GetNotificationSendList(string userId, dynamic obj, string sortField, string sortBy);
        void SaveNotificationSendList(string userId, string status, string title, string body);
    }
}