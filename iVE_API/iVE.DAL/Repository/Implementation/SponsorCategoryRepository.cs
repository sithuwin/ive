using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace iVE.DAL.Repository.Implementation {
    public class SponsorCategoryRepository : RepositoryBase<tblSponsorCategory>, ISponsorCategoryRepository {
        public SponsorCategoryRepository (AppDB repositoryContext) : base (repositoryContext) {

        }

        public dynamic GetSponsorCategory()
        {
            var main = (from s in RepositoryContext.SponsorCategories
                        where s.deleteFlag == false
                        orderby s.sponsorCat_seq
                        select new
                        {
                            SponserCategoryName = s.sponsorCat_name,
                            SponserCategoryID = s.sponsorCat_ID,
                        }).AsNoTracking().ToList();
            return main;
        }
    }
}