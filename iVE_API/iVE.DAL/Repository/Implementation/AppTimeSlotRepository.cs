using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
namespace iVE.DAL.Repository.Implementation
{
    public class AppTimeSlotRepository : RepositoryBase<tbl_App_TimeSlot>, IAppTimeSlotRepository
    {
        public AppTimeSlotRepository (AppDB repositoryContext) : base (repositoryContext) {

        }
    }
}