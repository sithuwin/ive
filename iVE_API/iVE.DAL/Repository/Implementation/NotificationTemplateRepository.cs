using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
namespace iVE.DAL.Repository.Implementation {
    public class NotificationTemplateRepository : RepositoryBase<tblNotificationTemplate>, INotificationTemplateRepository {
        public NotificationTemplateRepository (AppDB repositoryContext) : base (repositoryContext) {

        }

        public string GetNotificationData(string userName){
            
            string msg = (from c in RepositoryContext.NotificationTemplates
            where c.TitleMessage == NotificationType.NotifyVisitedVisitor
            select c.BodyMessage).FirstOrDefault();
            StringBuilder bodyMessage = new StringBuilder(msg);
            bodyMessage.Replace("<UserName>", userName);
            return bodyMessage.ToString();
        }
    }
}