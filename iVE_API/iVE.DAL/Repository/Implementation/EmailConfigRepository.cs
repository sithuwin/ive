using System;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
namespace iVE.DAL.Repository.Implementation
{
    public class EmailConfigRepository:RepositoryBase<tblConfigEmail>, IEmailConfigRepository
    {
        public EmailConfigRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
    }
}