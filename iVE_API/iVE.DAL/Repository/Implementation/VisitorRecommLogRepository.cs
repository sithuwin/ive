using System;
using System.Data;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using System.Linq;
namespace iVE.DAL.Repository.Implementation
{
    public class VisitorRecommLogRepository : RepositoryBase<tblVisitorRecommendationLog>, IVisitorRecommLogRepository
    {
        public VisitorRecommLogRepository(AppDB repositoryContext) : base (repositoryContext){

        }
    }
}