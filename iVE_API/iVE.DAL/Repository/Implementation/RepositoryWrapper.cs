using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
namespace iVE.DAL.Repository.Implementation
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private AppDB _repoContext;
        private ISiteRunNumberRepository _siteRunNumber;
        private IAdminRepository _admin;
        private IAccountRepository _account;
        private IShowConfigRepository _showConfig;
        private INotificationTemplateRepository _notificationTemplate;
        private INotificationSendListRepository _notificationSendList;
        private IAuditRepository _audit;
        //private IContactPersonRepository _contactPerson;
        private IExhibitorRepository _exhibitor;
        private IProductRepository _product;
        private ISponserRepository _sponser;
        private IBoothTemplateRepository _boothTemplate;
        private ITemplateRepository _template;
        private IProductCommantRepository _productCommant;
        private ISponsorCategoryRepository _sponsorCategory;
        private IFavouriteRepository _favourite;
        private IVisitorRecommendationRepository _visitorRecommendations;
        private IVisitorRecommLogRepository _visitorRecommLog;
        private IProductTypeRepository _productType;

        private ICountryRepository _country;

        private IAppointmentRepository _appointment;
        private IFeedBackBoardRepository _feedbackBoard;
        private ILeaderBoardRepository _leaderBoard;
        private IAccountLogicBlockerRepository _accountLogicBlocker;
        private IMCQScoreBoardRepository _mcqScoreBoard;
        private IEmailConfigRepository _emailConfig;
        private IAppTimeSlotRepository _appTimeSlot;
        private IChatMessageDetailsRepository _chatMessageDetails;
        public RepositoryWrapper(AppDB repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        public void Save()
        {
            _repoContext.SaveChanges();
        }
        public ISiteRunNumberRepository SiteRunNumber
        {
            get
            {
                if (_siteRunNumber == null)
                {
                    _siteRunNumber = new SiteRunNumberRepository(_repoContext);
                }
                return _siteRunNumber;
            }
        }

        public ICountryRepository Country
        {
            get
            {
                if (_country == null)
                {
                    _country = new CountryRepository(_repoContext);
                }
                return _country;
            }
        }

        public IVisitorRecommLogRepository VisitorRecommLog
        {
            get
            {
                if (_visitorRecommLog == null)
                {
                    _visitorRecommLog = new VisitorRecommLogRepository(_repoContext);
                }
                return _visitorRecommLog;
            }
        }
        public IAdminRepository Admin
        {
            get
            {
                if (_admin == null)
                {
                    _admin = new AdminRepository(_repoContext);
                }
                return _admin;
            }
        }

        public IAccountRepository Account
        {
            get
            {
                if (_account == null)
                {
                    _account = new AccountRepository(_repoContext);
                }
                return _account;
            }
        }

        public INotificationTemplateRepository NotificationTemplate
        {
            get
            {
                if (_notificationTemplate == null)
                {
                    _notificationTemplate = new NotificationTemplateRepository(_repoContext);
                }
                return _notificationTemplate;
            }
        }
        public INotificationSendListRepository NotificationSendList
        {
            get
            {
                if (_notificationSendList == null)
                {
                    _notificationSendList = new NotificationSendListRepository(_repoContext);
                }
                return _notificationSendList;
            }
        }

        public IAuditRepository Audit
        {
            get
            {
                if (_audit == null)
                {
                    _audit = new AuditRepository(_repoContext);
                }
                return _audit;
            }
        }
        //public IContactPersonRepository ContactPerson {
        //    get {
        //        if (_contactPerson == null) {
        //            _contactPerson = new ContactPersonRepository (_repoContext);
        //        }
        //        return _contactPerson;
        //    }
        //}
        public IExhibitorRepository Exhibitor
        {
            get
            {
                if (_exhibitor == null)
                {
                    _exhibitor = new ExhibitorRepository(_repoContext);
                }
                return _exhibitor;
            }
        }
        public IProductRepository Product
        {
            get
            {
                if (_product == null)
                {
                    _product = new ProductRepository(_repoContext);
                }
                return _product;
            }
        }
        public ISponserRepository Sponser
        {
            get
            {
                if (_sponser == null)
                {
                    _sponser = new SponserRepository(_repoContext);
                }
                return _sponser;
            }
        }
        public IBoothTemplateRepository BoothTemplate
        {
            get
            {
                if (_boothTemplate == null)
                {
                    _boothTemplate = new BoothTemplateRepository(_repoContext);
                }
                return _boothTemplate;
            }
        }
        public ITemplateRepository Template
        {
            get
            {
                if (_template == null)
                {
                    _template = new TemplateRepository(_repoContext);
                }
                return _template;
            }
        }
        public IProductCommantRepository ProductCommant
        {
            get
            {
                if (_productCommant == null)
                {
                    _productCommant = new ProductCommantRepository(_repoContext);
                }
                return _productCommant;
            }
        }

        public IShowConfigRepository ShowConfig
        {
            get
            {
                if (_showConfig == null)
                {
                    _showConfig = new ShowConfigRepository(_repoContext);
                }
                return _showConfig;
            }
        }

        public ISponsorCategoryRepository SponsorCategory
        {
            get
            {
                if (_sponsorCategory == null)
                {
                    _sponsorCategory = new SponsorCategoryRepository(_repoContext);
                }
                return _sponsorCategory;
            }
        }

        public IFavouriteRepository Favourite
        {
            get
            {
                if (_favourite == null)
                {
                    _favourite = new FavouriteRepository(_repoContext);
                }
                return _favourite;
            }
        }

        public IVisitorRecommendationRepository VisitorRecommendations
        {
            get
            {
                if (_visitorRecommendations == null)
                {
                    _visitorRecommendations = new VisitorRecommendationRepository(_repoContext);
                }
                return _visitorRecommendations;
            }
        }

        public IProductTypeRepository ProductType
        {
            get
            {
                if (_productType == null)
                {
                    _productType = new ProductTypeRepository(_repoContext);
                }
                return _productType;
            }
        }

        public IAppointmentRepository Appointment
        {
            get
            {
                if (_appointment == null)
                {
                    _appointment = new AppointmentRepository(_repoContext);
                }
                return _appointment;
            }
        }
        public IFeedBackBoardRepository FeedBackBoard
        {
            get
            {
                if (_feedbackBoard == null)
                {
                    _feedbackBoard = new FeedBackBoardRepository(_repoContext);
                }
                return _feedbackBoard;
            }
        }

        public ILeaderBoardRepository LeaderBoard
        {
            get
            {
                if (_leaderBoard == null)
                {
                    _leaderBoard = new LeaderBoardRepository(_repoContext);
                }
                return _leaderBoard;
            }
        }

        public IAccountLogicBlockerRepository AccountLogicBlocker
        {
            get
            {
                if (_accountLogicBlocker == null)
                {
                    _accountLogicBlocker = new AccountLoginBlockerRepository(_repoContext);
                }
                return _accountLogicBlocker;
            }
        }
        public IMCQScoreBoardRepository MCQScoreBoard
        {
            get
            {
                if (_mcqScoreBoard == null)
                {
                    _mcqScoreBoard = new MCQScoreBoardRepository(_repoContext);
                }
                return _mcqScoreBoard;
            }
        }

        public IEmailConfigRepository EmailConfig
        {
            get
            {
                if (_emailConfig == null)
                {
                    _emailConfig = new EmailConfigRepository(_repoContext);
                }
                return _emailConfig;
            }
        }

        public IAppTimeSlotRepository AppTimeSlot
        {
            get
            {
                if (_appTimeSlot == null)
                {
                    _appTimeSlot = new AppTimeSlotRepository(_repoContext);
                }
                return _appTimeSlot;
            }
        }

        public IChatMessageDetailsRepository ChatMessageDetails
        {
            get
            {
                if (_chatMessageDetails == null)
                {
                    _chatMessageDetails = new ChatMessageDetailsRepository(_repoContext);
                }
                return _chatMessageDetails;
            }
        }

    }
}