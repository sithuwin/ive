using System;
using System.Collections.Generic;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
namespace iVE.DAL.Repository.Implementation {
    public class AdminRepository : RepositoryBase<tbl_Administrator>, IAdminRepository {
        public AdminRepository (AppDB repositoryContext) : base (repositoryContext) {

        }

     
    }
}