using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
namespace iVE.DAL.Repository.Implementation
{
    public class ChatMessageDetailsRepository : RepositoryBase<tblChatMessageDetails>, IChatMessageDetailsRepository
    {
        public ChatMessageDetailsRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public void AddMessages(dynamic response)
        {
            for (int i = 0; i < response.Count; i++)
            {
                string messageId = response[i].id;
                string conversationId = response[i].conversationId;
                string receiverType = response[i].receiverType;
                string text = response[i].data.text;
                Int64 sentAt = response[i].sentAt;
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);


                tblChatMessageDetails oldChatMessageDetails = RepositoryContext.ChatMessageDetails.Where(x => x.messageId == messageId
                && x.conversationId == conversationId && x.receiverType == receiverType && x.text == text && x.sentAt == sentAt).FirstOrDefault();//
                if (oldChatMessageDetails == null)
                {
                    tblChatMessageDetails chatMessageDetails = new tblChatMessageDetails();
                    chatMessageDetails.messageId = response[i].id;
                    chatMessageDetails.conversationId = response[i].conversationId;
                    chatMessageDetails.receiverType = response[i].receiverType;
                    chatMessageDetails.category = response[i].category;
                    chatMessageDetails.type = response[i].type;
                    chatMessageDetails.text = response[i].data.text;
                    chatMessageDetails.senderId = response[i].data.entities.sender.entity.uid;
                    chatMessageDetails.receiverId = receiverType == "user" ? response[i].data.entities.receiver.entity.uid : response[i].data.entities.receiver.entity.guid;// Change 
                    chatMessageDetails.senderDetail = response[i].data.entities.sender.ToString();
                    chatMessageDetails.receiverDetail = response[i].data.entities.receiver.ToString();
                    chatMessageDetails.sentAt = response[i].sentAt;
                    chatMessageDetails.deliveredAt = response[i].deliveredAt;
                    chatMessageDetails.readAt = response[i].readAt;
                    chatMessageDetails.updatedAt = response[i].updatedAt;
                    chatMessageDetails.senderName = response[i].data.entities.sender.entity.name;
                    chatMessageDetails.receiverName = response[i].data.entities.receiver.entity.name;
                    chatMessageDetails.createdDate = DateTime.Now;
                    chatMessageDetails.updatedDate = DateTime.Now;
                    chatMessageDetails.sentDate = dtDateTime.AddSeconds(sentAt).ToLocalTime();
                    RepositoryContext.ChatMessageDetails.Add(chatMessageDetails);
                    RepositoryContext.SaveChanges();
                }
            }
        }
        public dynamic GetMessageList(dynamic obj, string sortField, string sortBy, bool isexport)
        {
            dynamic result;
            var mainQuery = (from e in RepositoryContext.ChatMessageDetails
                             select new
                             {
                                 id = e.Id,
                                 receiverType = e.receiverType,
                                 category = e.category,
                                 type = e.type,
                                 text = e.text,
                                 senderName = e.senderName,
                                 receiverName = e.receiverName,
                                 sentDate = e.sentDate
                             }).Distinct();
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForMessageList(mainQuery, obj);
                }
            }
            var objTotal = mainQuery.AsNoTracking().Count();
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.AsNoTracking().OrderBy(sortList);
            if (isexport)
            {
                return mainQuery.AsNoTracking().ToList();
            }
            mainQuery = PaginationSessionForMessageList(mainQuery, obj);

            var objResult = mainQuery.AsNoTracking().ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;

        }

        private dynamic FilterSessionForMessageList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(int),
                receiverType = default(string),
                category = default(string),
                type = default(string),
                text = default(string),
                senderName = default(string),
                receiverName = default(string),
                sentDate = default(DateTime?)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "senderName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.senderName.ToLower().Contains(Name));

                        }
                    }

                    if (filterName == "receiverName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.receiverName.ToLower().Contains(Name));

                        }
                    }

                    if (filterName == "receiverType")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.receiverType == Name);

                        }
                    }

                    if (filterName == "fromDate")
                    {
                        string date = filterValue;
                        if (!String.IsNullOrEmpty(date))
                        {
                            DateTime fDate = Convert.ToDateTime(date);
                            tmpQuery = tmpQuery.Where(x => x.sentDate >= fDate);

                        }
                    }

                    if (filterName == "toDate")
                    {
                        string date = filterValue;
                        if (!String.IsNullOrEmpty(date))
                        {
                            DateTime tDate = Convert.ToDateTime(date);
                            tmpQuery = tmpQuery.Where(x => ((DateTime)x.sentDate).Date <= tDate);
                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        private dynamic PaginationSessionForMessageList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(int),
                receiverType = default(string),
                category = default(string),
                type = default(string),
                text = default(string),
                senderName = default(string),
                receiverName = default(string),
                sentDate = default(DateTime?)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.id.ToString());
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.id.ToString()) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

    }
}