using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.CompilerServices;
namespace iVE.DAL.Repository.Implementation
{
    public class TemplateRepository : RepositoryBase<tblTemplate>, ITemplateRepository
    {
        public TemplateRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetAllBooth(string exhId)
        {
            var main = (from t in RepositoryContext.Templates
                        join e in RepositoryContext.Exhibitors on t.ExhID equals e.ExhID
                        where t.ExhID == exhId
                        select new
                        {
                            t.Image,
                            templateConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TemplateConfig>>(t.TemplateConfig),
                            t.TemplateID,
                            t.BoothID,
                            contactPersonStatus = e.ChatContactMembers != null && e.ChatContactMembers != "" ? true : false
                        }).ToList();
            return main;
        }

        public dynamic GetBoothById(string exhId, string boothId)
        {
            var main = (from tem in RepositoryContext.Templates
                        where tem.ExhID == exhId && tem.TemplateID == boothId
                        select new
                        {
                            tem.Image,
                            tem.ExhID,
                            templateConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TemplateConfig>>(tem.TemplateConfig),

                        }).FirstOrDefault();
            return main;
        }

        public dynamic GetBoothByTemplateId(string templateId)
        {
            var main = (from tem in RepositoryContext.Templates
            join e in RepositoryContext.Exhibitors on tem.ExhID equals e.ExhID
                        where tem.TemplateID == templateId
                        select new
                        {
                            tem.BoothID,
                            tem.ExhID,
                            tem.Image,
                            templateConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TemplateConfig>>(tem.TemplateConfig),
                            contactPersonStatus = e.ChatContactMembers != null && e.ChatContactMembers != "" ? true : false
                        }).AsNoTracking().FirstOrDefault();
            return main;
        }

        public void UpdateBoothById(tblTemplate template, dynamic obj)
        {

            template.BoothID = obj.boothTemplateId;
            string boothTemplateId = obj.boothTemplateId;
            tblBoothTemplate boothTemplate = RepositoryContext.BoothTemplates.Where(x => x.BoothID == boothTemplateId).Select(x => x).FirstOrDefault();
            if (boothTemplate != null)
            {
                template.TemplateConfig = boothTemplate.BoothConfig;
                template.Image = boothTemplate.BoothImage;
            }
            // template.Image = obj.image;
            RepositoryContext.Templates.Update(template);
            RepositoryContext.SaveChanges();
        }
        public void CreateBoothById(dynamic obj, string templateId, string exhId)
        {
            string boothTemplateId = obj.boothTemplateId;
            tblTemplate template = new tblTemplate();
            template.TemplateID = templateId;
            template.ExhID = exhId;
            template.BoothID = obj.boothTemplateId;
            // template.Image = obj.image;

            tblBoothTemplate boothTemplate = RepositoryContext.BoothTemplates.Where(x => x.BoothID == boothTemplateId).Select(x => x).FirstOrDefault();
            if (boothTemplate != null)
            {
                template.Image = boothTemplate.BoothImage;
                template.TemplateConfig = boothTemplate.BoothConfig;
            }

            RepositoryContext.Templates.Add(template);
            RepositoryContext.SaveChanges();
        }

        public void UpdateBoothConfigByName(string boothId, dynamic obj)
        {
            tblTemplate template = RepositoryContext.Templates.Where(x => x.TemplateID == boothId).Select(x => x).FirstOrDefault();
            string url = obj.url;
            string name = obj.name;
            string clickURLAction = obj.clickURLAction;
            string fileType = obj.fileType;
            string refID = obj.refId;
            if (template != null)
            {
                List<TemplateConfig> templateConfigs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TemplateConfig>>(template.TemplateConfig);
                List<TemplateConfig> listTemplateConfig = new List<TemplateConfig>();
                for (int i = 0; i < templateConfigs.Count(); i++)
                {
                    string cName = templateConfigs[i].name;
                    if (cName != name)
                    {
                        listTemplateConfig.Add(templateConfigs[i]);
                    }
                    else
                    {
                        TemplateConfig templateConfig = templateConfigs.Where(x => x.name == name).Select(x => x).FirstOrDefault();
                        templateConfig.image = url;
                        templateConfig.click_url_action = clickURLAction;
                        templateConfig.file_type = fileType;
                        templateConfig.refId = refID;
                        listTemplateConfig.Add(templateConfig);
                    }

                }
                template.TemplateConfig = Newtonsoft.Json.JsonConvert.SerializeObject(listTemplateConfig);
                RepositoryContext.Templates.Update(template);
                RepositoryContext.SaveChanges();
            }

        }

        /*8-7-2020*/
        public void UpdateTemplateImageByExhID(string exhId, tblTemplate template, dynamic obj)
        {
            template.Image = obj.url;
            RepositoryContext.Templates.Update(template);
            RepositoryContext.SaveChanges();
        }
    }
}