using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
namespace iVE.DAL.Repository.Implementation {
    public class BoothTemplateRepository : RepositoryBase<tblBoothTemplate>, IBoothTemplateRepository {
        public BoothTemplateRepository (AppDB repositoryContext) : base (repositoryContext) { }

        public dynamic GetAllBoothTemplate () {
            var main = (from bt in RepositoryContext.BoothTemplates select new {
                bt.BoothID,
                    bt.BoothName,
                    bt.BoothImage,
                    TemplateConfigs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TemplateConfig>> (bt.BoothConfig)
            }).ToList ();
            return main;
        }

        public dynamic GetBoothTemplateById (string Id) {
            var main = (from bt in RepositoryContext.BoothTemplates where bt.BoothID == Id select new {
                bt.BoothID,
                    bt.BoothName,
                    bt.BoothImage,
                    TemplateConfigs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TemplateConfig>> (bt.BoothConfig)
            }).FirstOrDefault ();
            return main;
        }

        public dynamic GetTemplateByExhId(string Id){
            var main = (from t in RepositoryContext.Templates 
            where t.ExhID == Id 
            select new
            {
                t.Image,
                t.TemplateConfig
            }).FirstOrDefault();
            return main;
        }
    }
}