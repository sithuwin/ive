using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
namespace iVE.DAL.Repository.Implementation {
    public class ShowConfigRepository : RepositoryBase<tblShowConfig>, IShowConfigRepository {
        public ShowConfigRepository (AppDB repositoryContext) : base (repositoryContext) {

        }

        public dynamic GetShowConfig () {
            var main = (from s in RepositoryContext.ShowConfigs 
                select new {
                Id = s.Id,
                Name = s.Name,
                Value = s.Value,
                Status = s.Status,
            }).ToList ();
            return main;
        }
        

        public void SaveNewShowConfig (dynamic obj, string name) {
            tblShowConfig showconfig = new tblShowConfig ();
            showconfig.Name = name;
            showconfig.Value = obj.Value;
            showconfig.Status = 1;

            RepositoryContext.ShowConfigs.Add (showconfig);
            RepositoryContext.SaveChanges ();

        }
        public void UpdateShowConfig (dynamic obj, tblShowConfig showconfig) {
            showconfig.Value = obj.Value;
            RepositoryContext.ShowConfigs.Update (showconfig);
            RepositoryContext.SaveChanges ();

        }
        public void DeleteShowConfig (tblShowConfig showconfig) {
            showconfig.Status = 0;
            RepositoryContext.ShowConfigs.Update (showconfig);
            RepositoryContext.SaveChanges ();
        }
    }
}