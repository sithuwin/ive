using System;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using System.Linq;
using System.Collections.Generic;
namespace iVE.DAL.Repository.Implementation
{
    public class ProductTypeRepository : RepositoryBase<tblProductTypeMaster>, IProductTypeRepository
    {
        public ProductTypeRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
        public dynamic GetProductTypeList()
        {
            List<dynamic> response = new List<dynamic>();
            var catList = (from pc in RepositoryContext.ProductTypeMaster select pc.ProductCategory).Distinct().ToArray();

            foreach (var cat in catList)
            {
                dynamic objResponse = new System.Dynamic.ExpandoObject();
                objResponse.productCategory = cat;
                List<tblProductTypeMaster> productTypes = (from pc in RepositoryContext.ProductTypeMaster where pc.ProductCategory == cat select pc).ToList();
                List<dynamic> proList = new List<dynamic>();
                for (int i = 0; i < productTypes.Count; i++)
                {
                    GroupDropdown newGroupDropdown = new GroupDropdown();
                    newGroupDropdown.value = productTypes[i].ProductID;
                    newGroupDropdown.viewValue = productTypes[i].ProductName;
                    proList.Add(newGroupDropdown);
                }
                objResponse.productTypeList = proList;
                response.Add(objResponse);
            }

            return response;
        }
    }
}