using System;
using System.Collections.Generic;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
namespace iVE.DAL.Repository.Implementation
{
    public class FeedBackBoardRepository: RepositoryBase<tblFeedbackboard>, IFeedBackBoardRepository
    {
        public FeedBackBoardRepository(AppDB repositoryContext):base(repositoryContext){

        }

        public dynamic GetFeedbacksbyThreadOwnerId(dynamic obj,string Id){
            //get all approved feedbacks by threadOwnerId
            dynamic result;
            string sortField = null; string sortBy = "";
            var mainQuery = (from f in RepositoryContext.Feedbackboards
            join c in RepositoryContext.UsersComments on f.FebId equals c.FebID
            join acc in RepositoryContext.Accounts on c.UserID equals acc.a_ID

            where f.deleteFlag == false && c.deleteFlag == false && acc.deleteFlag == false
            && c.Status == UserCommentStatus.Approved && f.ThreadOwnerID == Id
            select new
            {
                c.UserID,
                Name = acc.a_fullname,
                c.Comment
            }).Distinct();
            
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForAllFeedbacks(mainQuery, obj);
                }
            }

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "Name";
            if (sortBy == null || sortBy == "")
                sortBy = "desc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);
            var objTotal = mainQuery.Count();
            mainQuery = PaginationSessionForAllFeedbacks(mainQuery, obj);

            var objResult = mainQuery.ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }

        public dynamic GetAllComments(dynamic obj,string febId)
        {
            //get all user comments for organizer site for both Pending and Approved status
            dynamic result;
            string sortField = null; string sortBy = "";
            var mainQuery = (from c in RepositoryContext.UsersComments
            join f in RepositoryContext.Feedbackboards on c.FebID equals f.FebId
            join acc in RepositoryContext.Accounts on c.UserID equals acc.a_ID
            orderby f.FebId,acc.a_fullname
            where c.deleteFlag == false && f.deleteFlag == false && acc.deleteFlag == false
            && f.FebId == febId
            select new 
            {
                f.FebId,
                f.ThreadOwnerID,
                c.UserID,
                Name = acc.a_fullname,
                c.Comment,
                c.Status
            }).Distinct();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForAllUserComments(mainQuery, obj);
                }
            }

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "Name";
            if (sortBy == null || sortBy == "")
                sortBy = "desc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);
            var objTotal = mainQuery.Count();
            mainQuery = PaginationSessionForAllUserComments(mainQuery, obj);

            var objResult = mainQuery.ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }

        public void CreateComment(dynamic obj)
        {
            string febId = Convert.ToString(obj.febid);
            string userId = Convert.ToString(obj.userid);
            string comment = Convert.ToString(obj.comment);
            if (!string.IsNullOrEmpty(febId) && !string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(comment))
            {
                tblUsersComment objComment = new tblUsersComment();
                objComment.FebID = febId;
                objComment.UserID = userId;
                objComment.Comment = comment;
                objComment.Status = UserCommentStatus.Pending;
                objComment.CreatedDate = DateTime.Now;
                objComment.deleteFlag = false;
                RepositoryContext.UsersComments.Add(objComment);
                RepositoryContext.SaveChanges();
            }
        }

        public void CreateCommentActions(dynamic obj)
        {
            string userID = obj.userId;
            int commentID = Convert.ToInt16(obj.commentID);
            string userAction = obj.userAction;
            string desc = obj.description;

            if (!string.IsNullOrEmpty(userID) && !string.IsNullOrEmpty(Convert.ToString(commentID)) && !string.IsNullOrEmpty(userAction))
            {
                tblUsersCommentAction objCommAction = new tblUsersCommentAction();
                objCommAction.UserId = userID;
                objCommAction.CommentID = commentID;
                objCommAction.UserActions = userAction;
                objCommAction.Description = desc;
                objCommAction.CreatedDate = DateTime.Now;
                objCommAction.deleteFlag = false;
                RepositoryContext.UsersCommentActions.Add(objCommAction);
                RepositoryContext.SaveChanges();
            }
        }

        public void UpdateCommentStatus(string febId, string cmtId, dynamic obj,string loginId)
        {
            //update the comment status to Cancelled/Approved by current login userId that is still pending
            string cmtStatus = obj.status;
            tblUsersComment cmtObj = new tblUsersComment();
            cmtObj = (from c in RepositoryContext.UsersComments
                          where c.deleteFlag == false && c.FebID == febId
                          && c.Status == UserCommentStatus.Pending && c.CommentID == Convert.ToInt16(cmtId)
                          select c).FirstOrDefault();

            if (!string.IsNullOrEmpty(cmtStatus) && cmtObj != null)
            {
                cmtObj.Status = cmtStatus;
                cmtObj.ModifiedDate = DateTime.Now;
                cmtObj.UpdatedBy = loginId;
                RepositoryContext.UsersComments.Update(cmtObj);
                RepositoryContext.SaveChanges();
            }
        }

        #region  Filter Session
        private dynamic FilterSessionForAllFeedbacks(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserID = default(string),
                Name = default(string),
                Comment = default(string)
  
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "Name")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.Name.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic FilterSessionForAllUserComments(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                FebId = default(string),
                ThreadOwnerID = default(string),
                UserID = default(string),
                Name = default(string),
                Comment = default(string),
                Status = default(string)
  
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "Name")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.Name.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        #endregion

        #region  Pagination Session
        private dynamic PaginationSessionForAllFeedbacks(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserID = default(string),
                Name = default(string),
                Comment = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.UserID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.UserID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForAllUserComments(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                FebId = default(string),
                ThreadOwnerID = default(string),
                UserID = default(string),
                Name = default(string),
                Comment = default(string),
                Status = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.FebId);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.FebId) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }
        #endregion

    }
}