using System;
using System.Collections.Generic;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
namespace iVE.DAL.Repository.Implementation
{
    public class LeaderBoardRepository: RepositoryBase<tblLeaderBoard>, ILeaderBoardRepository
    {
        public LeaderBoardRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetAllLeaderBoard(dynamic obj,string returnUrl){
            dynamic result;
            string sortField = null; string sortBy = "";
            var mainQuery = (from c in RepositoryContext.LeaderBoards
                             join acc in RepositoryContext.Accounts on c.ParticipantID equals acc.a_ID
                             join cl in (from s in RepositoryContext.Countries where s.deleteFlag == false
                             select new { s.ISO3Digit, s.CountryName }) on acc.a_country equals cl.ISO3Digit into t
                             from clist in t.DefaultIfEmpty()
                             where c.deleteFlag == false && acc.deleteFlag == false
                             select new
                             {
                                 c.ParticipantID,
                                 Name = acc.a_fullname,
                                 ParticipantPhoto = (acc.ProfilePic == null || acc.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + acc.ProfilePic)),
                                 Designation = acc.a_designation,
                                 company = acc.a_company,
                                 country = clist.CountryName,
                                 TotalPoints = c.TotalPoints
                             }).Distinct();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForAllLeaderBoard(mainQuery, obj);
                }
            }

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "TotalPoints";
            if (sortBy == null || sortBy == "")
                sortBy = "desc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);
            var objTotal = mainQuery.Count();
            mainQuery = PaginationSessionForAllLeaderBoard(mainQuery, obj);

            var objResult = mainQuery.ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved." };
            return result;
        }

        public dynamic GetAllEntitlePoints(dynamic obj){
            dynamic result;
            string sortField = null; string sortBy = "";
            var mainQuery = (from c in RepositoryContext.LeaderBoardEntitlePoints
            where c.deleteFlag == false
            select new
            {
                c.PointKey,
                displayText = c.LeaderBoardCat,
                //c.Description,
                c.Points
            }).Distinct();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForAllEntitlePoints(mainQuery, obj);
                }
            }

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }

            if (sortField == null || sortField == "")
                sortField = "PointKey";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);
            var objTotal = mainQuery.Count();
            mainQuery = PaginationSessionForAllEntitlePoints(mainQuery, obj);

            var objResult = mainQuery.ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }

        #region  Filter Session
        private dynamic FilterSessionForAllLeaderBoard(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ParticipantID = default(string),
                Name = default(string),
                ParticipantPhoto = default(string),
                Designation = default(string),
                company = default(string),
                country = default(string),
                TotalPoints = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "Name")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.Name.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        private dynamic FilterSessionForAllEntitlePoints(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                PointKey = default(int),
                //LeaderBoardCat = default(string),
                displayText = default(string),
                Points = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "LeaderBoardCat")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.displayText.Contains(Name));

                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        #endregion

        #region  Pagination Session
        private dynamic PaginationSessionForAllLeaderBoard(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ParticipantID = default(string),
                Name = default(string),
                ParticipantPhoto = default(string),
                Designation = default(string),
                company = default(string),
                country = default(string),
                TotalPoints = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.Name);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.Name) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        private dynamic PaginationSessionForAllEntitlePoints(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                PointKey = default(int),
                //LeaderBoardCat = default(string),
                displayText = default(string),
                Points = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.PointKey.ToString());
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.PointKey.ToString()) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        #endregion
    }
}