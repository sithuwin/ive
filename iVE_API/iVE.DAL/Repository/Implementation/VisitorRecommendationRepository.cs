using System;
using System.Data;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using System.Linq;
using iVE.DAL.Util;
using Microsoft.EntityFrameworkCore;

namespace iVE.DAL.Repository.Implementation
{
    public class VisitorRecommendationRepository : RepositoryBase<tblVisitorRecommendations>, IVisitorRecommendationRepository
    {
        public VisitorRecommendationRepository (AppDB repositoryContext) : base (repositoryContext){

        }

        public void AddVisitorRecommendation(string vId, string exhID)
        {
            tblVisitorRecommendations obj = new tblVisitorRecommendations();
            obj.VisitorID = vId;
            obj.ExhID = exhID;
            obj.CreatedDate = DateTime.Now;
            RepositoryContext.VisitorRecommendations.Add(obj);
            RepositoryContext.SaveChanges();
        }

        public dynamic GetAllRecommendedExhibitorsByVisitor(string vId)
        {
            var main = (from vr in RepositoryContext.VisitorRecommendations
                        join ex in RepositoryContext.Exhibitors on vr.ExhID equals ex.ExhID
                        where vr.VisitorID == vId
                        && ex.Status == ExhibitorStatus.Published
                        select new
                        {
                            ex.BoothNo,
                            ex.BoothTemplate,
                            ex.CompanyName,
                            ex.Country,
                            ex.Description,
                            ex.ExhCategory,
                            ex.ExhID,
                            ex.ExhType,
                            ex.HallNo
                        }).AsNoTracking().ToList();
            return main;
        }

        public void AddVisitorRecommendationLog(tblVisitorRecommendations vistorRec)
        {
            tblVisitorRecommendationLog obj = new tblVisitorRecommendationLog();
            obj.VisitorID = vistorRec.VisitorID;
            obj.ExhID = vistorRec.ExhID;
            obj.CreatedDate = vistorRec.CreatedDate;
            RepositoryContext.VisitorRecommendationLog.Add(obj);
            RepositoryContext.SaveChanges();
        }
         
    }
}