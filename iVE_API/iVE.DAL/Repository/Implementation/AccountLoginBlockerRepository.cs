using System;
using System.Collections.Generic;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;

using Microsoft.Extensions.Configuration;
namespace iVE.DAL.Repository.Implementation
{
    public class AccountLoginBlockerRepository : RepositoryBase<tblAccountLoginBlocker>, IAccountLogicBlockerRepository
    {
        public AccountLoginBlockerRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
    }
}