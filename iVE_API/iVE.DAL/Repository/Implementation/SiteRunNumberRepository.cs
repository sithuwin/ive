using System;
using System.Collections.Generic;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
namespace iVE.DAL.Repository.Implementation
{
    public class SiteRunNumberRepository : RepositoryBase<SiteRunNumber>, ISiteRunNumberRepository
    {
        public SiteRunNumberRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public string GetNewId(string runKey)
        {
            SiteRunNumber siteRun = (from s in RepositoryContext.SiteRunNumbers where s.prefix == runKey select s).FirstOrDefault();
            return siteRun.prefix + siteRun.runno_curr;
        }

        public SiteRunNumber GetRunNumberPrefixAndCurrentNo(string runKey)
        {
            SiteRunNumber siteRun = (from s in RepositoryContext.SiteRunNumbers where s.prefix == runKey select s).FirstOrDefault();
            return siteRun;
        }

        public void UpdateNumberByRunKey(string runKey)
        {
            SiteRunNumber siteRun = (from s in RepositoryContext.SiteRunNumbers where s.prefix == runKey select s).FirstOrDefault();

            siteRun.runno_curr += siteRun.runno_incr;
            Update(siteRun);
            RepositoryContext.SaveChanges();

        }

        public void UpdateNumberByRunKeyAndCurrentNo(string runKey, int currentNo)
        {
            SiteRunNumber siteRun = (from s in RepositoryContext.SiteRunNumbers where s.prefix == runKey select s).FirstOrDefault();

            siteRun.runno_curr += currentNo;
            Update(siteRun);
            RepositoryContext.SaveChanges();

        }
    }
}