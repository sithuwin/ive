using System;
using System.Linq;
using iVE.DAL.Models;
using iVE.DAL.Repository.Interface;
using iVE.DAL.Util;
using Microsoft.EntityFrameworkCore;

namespace iVE.DAL.Repository.Implementation
{
    public class CountryRepository : RepositoryBase<tbl_Country>, ICountryRepository
    {
        public CountryRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetCountryList (){
            var main = (from c in RepositoryContext.Countries
            where c.deleteFlag == false
            orderby c.CountryName
            select new 
            { 
                ISO3Digit = c.ISO3Digit, 
                CountryName = c.CountryName,
                CountryCode = c.CountryCode,
                Region = c.Region != null && c.Region != "" ? c.Region : "" 
            }).AsNoTracking().ToList();
            return main;
        }

        public string GetCountryISO3DigitByCountry (string countryName){
            var main = (from c in RepositoryContext.Countries
            where c.deleteFlag == false
            && c.CountryName == countryName
            select new 
            { 
                c.ISO3Digit
            }).FirstOrDefault();
            return main.ISO3Digit;
        }
    }
}